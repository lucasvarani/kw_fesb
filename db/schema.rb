# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20190419553543) do

  create_table "award_galleries", :force => true do |t|
    t.string   "name"
    t.boolean  "active",                 :default => true, :null => false
    t.boolean  "published",              :default => true, :null => false
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "award_img_file_name"
    t.string   "award_img_content_type"
    t.integer  "award_img_file_size"
    t.datetime "award_img_updated_at"
    t.integer  "award_id"
  end

  create_table "awards", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",                   :default => true, :null => false
    t.boolean  "published",                :default => true, :null => false
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.string   "award_image_file_name"
    t.string   "award_image_content_type"
    t.integer  "award_image_file_size"
    t.datetime "award_image_updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.boolean  "active",                    :default => true,  :null => false
    t.boolean  "published",                 :default => true,  :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "category_img_file_name"
    t.string   "category_img_content_type"
    t.integer  "category_img_file_size"
    t.datetime "category_img_updated_at"
    t.boolean  "show_item",                 :default => false, :null => false
  end

  create_table "cities", :force => true do |t|
    t.string   "state"
    t.string   "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cities", ["state"], :name => "idx_1"

  create_table "class_categories", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true, :null => false
    t.boolean  "published",  :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "class_images", :force => true do |t|
    t.integer  "class_name_id"
    t.string   "name"
    t.boolean  "active",                    :default => true, :null => false
    t.boolean  "published",                 :default => true, :null => false
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "class_images_file_name"
    t.string   "class_images_content_type"
    t.integer  "class_images_file_size"
    t.datetime "class_images_updated_at"
  end

  add_index "class_images", ["active", "class_name_id", "published"], :name => "idx_1"
  add_index "class_images", ["class_name_id"], :name => "idx_2"

  create_table "class_names", :force => true do |t|
    t.integer  "class_category_id"
    t.string   "name"
    t.string   "duration"
    t.string   "period"
    t.string   "number_of_vacancies"
    t.text     "description"
    t.text     "labour_market"
    t.boolean  "active",                         :default => true,  :null => false
    t.boolean  "published",                      :default => true,  :null => false
    t.datetime "created_at",                                        :null => false
    t.datetime "updated_at",                                        :null => false
    t.string   "class_icon_file_name"
    t.string   "class_icon_content_type"
    t.integer  "class_icon_file_size"
    t.datetime "class_icon_updated_at"
    t.string   "class_header_file_name"
    t.string   "class_header_content_type"
    t.integer  "class_header_file_size"
    t.datetime "class_header_updated_at"
    t.string   "class_footer_file_name"
    t.string   "class_footer_content_type"
    t.integer  "class_footer_file_size"
    t.datetime "class_footer_updated_at"
    t.integer  "teacher_id"
    t.string   "class_matrix_file_name"
    t.string   "class_matrix_content_type"
    t.integer  "class_matrix_file_size"
    t.datetime "class_matrix_updated_at"
    t.integer  "class_style_id"
    t.text     "objective"
    t.text     "differential"
    t.text     "target"
    t.text     "requirements"
    t.boolean  "pre_subscribe",                  :default => false, :null => false
    t.string   "cdd"
    t.string   "embed"
    t.integer  "father_id"
    t.boolean  "show_subscribe",                 :default => true,  :null => false
    t.string   "pha"
    t.string   "summary_content_file_name"
    t.string   "summary_content_content_type"
    t.integer  "summary_content_file_size"
    t.datetime "summary_content_updated_at"
    t.string   "president_gallery_file_name"
    t.string   "president_gallery_content_type"
    t.integer  "president_gallery_file_size"
    t.datetime "president_gallery_updated_at"
    t.string   "price"
    t.string   "financing_6"
    t.string   "financing_8"
    t.string   "financing_10"
    t.boolean  "show_remaining",                 :default => true,  :null => false
  end

  add_index "class_names", ["active", "class_category_id", "published"], :name => "idx_1"

  create_table "class_styles", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true, :null => false
    t.boolean  "published",  :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.integer  "department_id"
    t.text     "message"
    t.boolean  "active",        :default => true, :null => false
    t.boolean  "published",     :default => true, :null => false
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "departments", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.boolean  "active",     :default => true, :null => false
    t.boolean  "published",  :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "enems", :force => true do |t|
    t.string   "name"
    t.string   "cell"
    t.string   "cpf"
    t.integer  "nota"
    t.string   "curso"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "email"
  end

  create_table "essays", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "address"
    t.string   "cpf"
    t.string   "cell"
    t.text     "essay"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "class"
  end

  create_table "ex_alunos", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "subject"
    t.string   "phone"
    t.string   "message"
    t.string   "curso",           :null => false
    t.string   "conclusion_year", :null => false
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.boolean  "w_area"
    t.string   "work_area"
  end

  create_table "exam_dates", :force => true do |t|
    t.datetime "exam_date"
    t.integer  "exam_id"
    t.string   "exam_city",         :limit => 45
    t.boolean  "published",                       :default => true
    t.boolean  "active",                          :default => true
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.boolean  "view_notes",                      :default => false, :null => false
    t.integer  "itinerant_exam_id"
  end

  create_table "exam_dates_2016", :force => true do |t|
    t.datetime "exam_date"
    t.integer  "exam_id"
    t.boolean  "published",  :default => true
    t.boolean  "active",     :default => true
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "view_notes", :default => false, :null => false
  end

  create_table "exams", :force => true do |t|
    t.string   "name"
    t.integer  "exam_type_id"
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "published",    :default => false
    t.boolean  "active",       :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "view_notes",   :default => false, :null => false
    t.integer  "year",         :default => 2015
  end

  create_table "exams_2016", :force => true do |t|
    t.string   "name"
    t.integer  "exam_type_id"
    t.text     "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "published",    :default => false
    t.boolean  "active",       :default => true
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "view_notes",   :default => false, :null => false
    t.integer  "year",         :default => 2015
  end

  create_table "file_imports", :force => true do |t|
    t.text     "log",                     :limit => 2147483647
    t.text     "error",                   :limit => 2147483647
    t.integer  "user_id"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "fileimport_file_name"
    t.string   "fileimport_content_type"
    t.integer  "fileimport_file_size"
    t.datetime "fileimport_updated_at"
  end

  create_table "highlights", :force => true do |t|
    t.string   "name"
    t.integer  "flag"
    t.string   "link"
    t.string   "target"
    t.boolean  "active",                      :default => true, :null => false
    t.boolean  "published",                   :default => true, :null => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "high_img_file_name"
    t.string   "high_img_content_type"
    t.integer  "high_img_file_size"
    t.datetime "high_img_updated_at"
    t.string   "highlight_file_file_name"
    t.string   "highlight_file_content_type"
    t.integer  "highlight_file_file_size"
    t.datetime "highlight_file_updated_at"
  end

  create_table "images", :force => true do |t|
    t.string   "alt",               :default => ""
    t.string   "hint",              :default => ""
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "item_images", :force => true do |t|
    t.integer  "item_id"
    t.string   "name"
    t.boolean  "active",                :default => true, :null => false
    t.boolean  "published",             :default => true, :null => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "item_img_file_name"
    t.string   "item_img_content_type"
    t.integer  "item_img_file_size"
    t.datetime "item_img_updated_at"
  end

  add_index "item_images", ["item_id"], :name => "idx_1"

  create_table "items", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.boolean  "active",                 :default => true, :null => false
    t.boolean  "published",              :default => true, :null => false
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "category_id"
    t.string   "item_file_file_name"
    t.string   "item_file_content_type"
    t.integer  "item_file_file_size"
    t.datetime "item_file_updated_at"
  end

  add_index "items", ["category_id"], :name => "idx_1"

  create_table "itinerant_exams", :force => true do |t|
    t.string   "city"
    t.string   "phone"
    t.string   "address"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "librarie_tab_names", :force => true do |t|
    t.string "name"
  end

  create_table "pre_subscribes", :force => true do |t|
    t.integer  "class_name_id"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.boolean  "active",               :default => true, :null => false
    t.boolean  "published",            :default => true, :null => false
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "rg"
    t.string   "cpf"
    t.string   "social_name"
    t.string   "graduate_class"
    t.string   "graduate_year"
    t.string   "graduate_institution"
    t.string   "birthday"
  end

  create_table "recipes", :force => true do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "recipe_img_file_name"
    t.string   "recipe_img_content_type"
    t.integer  "recipe_img_file_size"
    t.datetime "recipe_img_updated_at"
  end

  create_table "redacaos", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "address"
    t.string   "cpf"
    t.string   "cell"
    t.text     "essay"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "curso"
  end

  create_table "registrations", :force => true do |t|
    t.string   "subscribe_id"
    t.string   "name"
    t.string   "cpf"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.boolean  "active",                        :default => true, :null => false
    t.boolean  "published",                     :default => true, :null => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.string   "scheduling"
    t.string   "phone"
    t.string   "comprovante_enem_file_name"
    t.string   "comprovante_enem_content_type"
    t.integer  "comprovante_enem_file_size"
    t.datetime "comprovante_enem_updated_at"
    t.string   "arquivo_rg_file_name"
    t.string   "arquivo_rg_content_type"
    t.integer  "arquivo_rg_file_size"
    t.datetime "arquivo_rg_updated_at"
    t.string   "arquivo_rg_verso_file_name"
    t.string   "arquivo_rg_verso_content_type"
    t.integer  "arquivo_rg_verso_file_size"
    t.datetime "arquivo_rg_verso_updated_at"
  end

  create_table "registrations_2016", :force => true do |t|
    t.string   "subscribe_id"
    t.string   "name"
    t.string   "cpf"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.boolean  "active",                        :default => true, :null => false
    t.boolean  "published",                     :default => true, :null => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.date     "scheduling"
    t.string   "phone"
    t.string   "comprovante_enem_file_name"
    t.string   "comprovante_enem_content_type"
    t.integer  "comprovante_enem_file_size"
    t.datetime "comprovante_enem_updated_at"
    t.string   "arquivo_rg_file_name"
    t.string   "arquivo_rg_content_type"
    t.integer  "arquivo_rg_file_size"
    t.datetime "arquivo_rg_updated_at"
  end

  add_index "registrations_2016", ["active"], :name => "idx_1"

  create_table "registrations_2017", :force => true do |t|
    t.string   "subscribe_id"
    t.string   "name"
    t.string   "cpf"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.boolean  "active",                        :default => true, :null => false
    t.boolean  "published",                     :default => true, :null => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.date     "scheduling"
    t.string   "phone"
    t.string   "comprovante_enem_file_name"
    t.string   "comprovante_enem_content_type"
    t.integer  "comprovante_enem_file_size"
    t.datetime "comprovante_enem_updated_at"
    t.string   "arquivo_rg_file_name"
    t.string   "arquivo_rg_content_type"
    t.integer  "arquivo_rg_file_size"
    t.datetime "arquivo_rg_updated_at"
    t.string   "arquivo_rg_verso_file_name"
    t.string   "arquivo_rg_verso_content_type"
    t.integer  "arquivo_rg_verso_file_size"
    t.datetime "arquivo_rg_verso_updated_at"
  end

  add_index "registrations_2017", ["active"], :name => "idx_1"

  create_table "registrations_2018", :force => true do |t|
    t.string   "subscribe_id"
    t.string   "name"
    t.string   "cpf"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.boolean  "active",                        :default => true, :null => false
    t.boolean  "published",                     :default => true, :null => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.date     "scheduling"
    t.string   "phone"
    t.string   "comprovante_enem_file_name"
    t.string   "comprovante_enem_content_type"
    t.integer  "comprovante_enem_file_size"
    t.datetime "comprovante_enem_updated_at"
    t.string   "arquivo_rg_file_name"
    t.string   "arquivo_rg_content_type"
    t.integer  "arquivo_rg_file_size"
    t.datetime "arquivo_rg_updated_at"
    t.string   "arquivo_rg_verso_file_name"
    t.string   "arquivo_rg_verso_content_type"
    t.integer  "arquivo_rg_verso_file_size"
    t.datetime "arquivo_rg_verso_updated_at"
  end

  create_table "registrations_bkp_dez_16", :force => true do |t|
    t.string   "subscribe_id"
    t.string   "name"
    t.string   "cpf"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.boolean  "active",                        :default => true, :null => false
    t.boolean  "published",                     :default => true, :null => false
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.date     "scheduling"
    t.string   "phone"
    t.string   "comprovante_enem_file_name"
    t.string   "comprovante_enem_content_type"
    t.integer  "comprovante_enem_file_size"
    t.datetime "comprovante_enem_updated_at"
    t.string   "arquivo_rg_file_name"
    t.string   "arquivo_rg_content_type"
    t.integer  "arquivo_rg_file_size"
    t.datetime "arquivo_rg_updated_at"
  end

  add_index "registrations_bkp_dez_16", ["active"], :name => "idx_1"

  create_table "roles", :force => true do |t|
    t.string   "name",              :limit => 40
    t.string   "authorizable_type", :limit => 40
    t.integer  "authorizable_id"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "roles_users", ["role_id", "user_id"], :name => "idx_1"

  create_table "schedules", :force => true do |t|
    t.string   "name"
    t.date     "schedule_date"
    t.time     "schedule_time"
    t.string   "description"
    t.boolean  "published",           :default => true, :null => false
    t.boolean  "active",              :default => true, :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
    t.date     "schedule_date_end"
  end

  add_index "schedules", ["active", "published", "schedule_date"], :name => "idx_1"

  create_table "scientific_initiations", :force => true do |t|
    t.string   "name"
    t.boolean  "active",            :default => true, :null => false
    t.boolean  "published",         :default => true, :null => false
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "sheets", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "ra"
    t.string   "author"
    t.string   "advisor"
    t.string   "title_subtitle"
    t.string   "course"
    t.integer  "number_pages"
    t.integer  "number_illustrations"
    t.string   "keywords"
    t.text     "additional_information"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.integer  "class_name_id"
    t.string   "code_name"
  end

  create_table "simple_captcha_data", :force => true do |t|
    t.string   "key",        :limit => 40
    t.string   "value",      :limit => 6
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
  end

  add_index "simple_captcha_data", ["key"], :name => "idx_key"

  create_table "socioeconomics", :force => true do |t|
    t.string   "birth_state"
    t.string   "birth_city"
    t.string   "income"
    t.string   "income_family"
    t.string   "school"
    t.string   "highschool"
    t.string   "occupation"
    t.string   "how_did_me"
    t.string   "best_ad"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "city_id"
  end

  create_table "socioeconomics_2016", :force => true do |t|
    t.string   "birth_state"
    t.string   "birth_city"
    t.string   "income"
    t.string   "income_family"
    t.string   "school"
    t.string   "highschool"
    t.string   "occupation"
    t.string   "how_did_me"
    t.string   "best_ad"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "city_id"
  end

  create_table "subscribes", :force => true do |t|
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "user_id"
    t.decimal  "price",                           :precision => 10, :scale => 0
    t.string   "ticket_number"
    t.datetime "created_at",                                                                        :null => false
    t.datetime "updated_at",                                                                        :null => false
    t.text     "ticket_link"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.integer  "exam_date_id"
    t.string   "registration"
    t.boolean  "paid",                                                           :default => false, :null => false
    t.decimal  "copywriting_note",                :precision => 10, :scale => 4
    t.decimal  "note_issues",                     :precision => 10, :scale => 4
    t.decimal  "copywriting_weight",              :precision => 10, :scale => 4
    t.decimal  "weight_issues",                   :precision => 10, :scale => 4
    t.decimal  "endnote",                         :precision => 10, :scale => 5
    t.boolean  "attended",                                                       :default => false, :null => false
    t.string   "subscribe_message"
    t.integer  "year",                                                           :default => 2015
    t.boolean  "registrationshow",                                               :default => false, :null => false
    t.integer  "confirmation",       :limit => 1
    t.string   "class_room"
  end

  create_table "subscribes_2016", :force => true do |t|
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "user_id"
    t.decimal  "price",              :precision => 10, :scale => 0
    t.string   "ticket_number"
    t.datetime "created_at",                                                           :null => false
    t.datetime "updated_at",                                                           :null => false
    t.text     "ticket_link"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.integer  "exam_date_id"
    t.string   "registration"
    t.boolean  "paid",                                              :default => false, :null => false
    t.decimal  "copywriting_note",   :precision => 10, :scale => 4
    t.decimal  "note_issues",        :precision => 10, :scale => 4
    t.decimal  "copywriting_weight", :precision => 10, :scale => 4
    t.decimal  "weight_issues",      :precision => 10, :scale => 4
    t.decimal  "endnote",            :precision => 10, :scale => 5
    t.boolean  "attended",                                          :default => false, :null => false
    t.string   "subscribe_message"
    t.integer  "year",                                              :default => 2015
    t.boolean  "registrationshow",                                  :default => false, :null => false
  end

  add_index "subscribes_2016", ["endnote", "year"], :name => "idx_7"
  add_index "subscribes_2016", ["endnote"], :name => "idx_5"
  add_index "subscribes_2016", ["exam_date_id", "exam_id"], :name => "idx_1"
  add_index "subscribes_2016", ["exam_id"], :name => "idx_2"
  add_index "subscribes_2016", ["paid"], :name => "idx_4"
  add_index "subscribes_2016", ["registrationshow", "year"], :name => "idx_8"
  add_index "subscribes_2016", ["user_id"], :name => "idx_3"
  add_index "subscribes_2016", ["year"], :name => "idx_6"

  create_table "subscribes_2017", :force => true do |t|
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "user_id"
    t.decimal  "price",              :precision => 10, :scale => 0
    t.string   "ticket_number"
    t.datetime "created_at",                                                           :null => false
    t.datetime "updated_at",                                                           :null => false
    t.text     "ticket_link"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.integer  "exam_date_id"
    t.string   "registration"
    t.boolean  "paid",                                              :default => false, :null => false
    t.decimal  "copywriting_note",   :precision => 10, :scale => 4
    t.decimal  "note_issues",        :precision => 10, :scale => 4
    t.decimal  "copywriting_weight", :precision => 10, :scale => 4
    t.decimal  "weight_issues",      :precision => 10, :scale => 4
    t.decimal  "endnote",            :precision => 10, :scale => 5
    t.boolean  "attended",                                          :default => false, :null => false
    t.string   "subscribe_message"
    t.integer  "year",                                              :default => 2015
    t.boolean  "registrationshow",                                  :default => false, :null => false
  end

  add_index "subscribes_2017", ["endnote", "year"], :name => "idx_7"
  add_index "subscribes_2017", ["endnote"], :name => "idx_5"
  add_index "subscribes_2017", ["exam_date_id", "exam_id"], :name => "idx_1"
  add_index "subscribes_2017", ["exam_id"], :name => "idx_2"
  add_index "subscribes_2017", ["paid"], :name => "idx_4"
  add_index "subscribes_2017", ["registrationshow", "year"], :name => "idx_8"
  add_index "subscribes_2017", ["user_id"], :name => "idx_3"
  add_index "subscribes_2017", ["year"], :name => "idx_6"

  create_table "subscribes_2018", :force => true do |t|
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "user_id"
    t.decimal  "price",                           :precision => 10, :scale => 0
    t.string   "ticket_number"
    t.datetime "created_at",                                                                        :null => false
    t.datetime "updated_at",                                                                        :null => false
    t.text     "ticket_link"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.integer  "exam_date_id"
    t.string   "registration"
    t.boolean  "paid",                                                           :default => false, :null => false
    t.decimal  "copywriting_note",                :precision => 10, :scale => 4
    t.decimal  "note_issues",                     :precision => 10, :scale => 4
    t.decimal  "copywriting_weight",              :precision => 10, :scale => 4
    t.decimal  "weight_issues",                   :precision => 10, :scale => 4
    t.decimal  "endnote",                         :precision => 10, :scale => 5
    t.boolean  "attended",                                                       :default => false, :null => false
    t.string   "subscribe_message"
    t.integer  "year",                                                           :default => 2015
    t.boolean  "registrationshow",                                               :default => false, :null => false
    t.integer  "confirmation",       :limit => 1
    t.string   "class_room"
  end

  create_table "teachers", :force => true do |t|
    t.string   "name"
    t.string   "curriculum_vitae"
    t.boolean  "active",                     :default => true, :null => false
    t.boolean  "published",                  :default => true, :null => false
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.string   "teacher_image_file_name"
    t.string   "teacher_image_content_type"
    t.integer  "teacher_image_file_size"
    t.datetime "teacher_image_updated_at"
  end

  add_index "teachers", ["active", "published"], :name => "idx_1"

  create_table "teaching_bodies", :force => true do |t|
    t.integer  "class_name_id"
    t.integer  "teacher_id"
    t.boolean  "coordinator",   :default => false, :null => false
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  add_index "teaching_bodies", ["class_name_id"], :name => "idx_1"

  create_table "transfers", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "city"
    t.string   "phone"
    t.string   "curso"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "type_warnings", :force => true do |t|
    t.string   "name"
    t.boolean  "active",     :default => true, :null => false
    t.boolean  "published",  :default => true, :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name",                                     :null => false
    t.string   "last_name"
    t.string   "email",                                    :null => false
    t.string   "crypted_password",                         :null => false
    t.string   "password_salt",                            :null => false
    t.string   "persistence_token",                        :null => false
    t.string   "perishable_token",                         :null => false
    t.integer  "login_count",           :default => 0,     :null => false
    t.integer  "failed_login_count",    :default => 0,     :null => false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "sex"
    t.string   "place_of_birth"
    t.datetime "birth_date"
    t.string   "civil_state"
    t.string   "phone"
    t.string   "cell_phone"
    t.string   "rg"
    t.string   "rg_oe"
    t.string   "rg_ee"
    t.string   "cpf"
    t.string   "rne"
    t.string   "high_school"
    t.boolean  "ex_student"
    t.boolean  "special_needs"
    t.string   "special_needs_visual"
    t.string   "special_needs_phisics"
    t.string   "special_needs_hearing"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.boolean  "cms_user",              :default => false
    t.integer  "nota_enem"
    t.string   "social_name"
  end

  create_table "users_2016", :force => true do |t|
    t.string   "name",                                     :null => false
    t.string   "last_name"
    t.string   "email",                                    :null => false
    t.string   "crypted_password",                         :null => false
    t.string   "password_salt",                            :null => false
    t.string   "persistence_token",                        :null => false
    t.string   "perishable_token",                         :null => false
    t.integer  "login_count",           :default => 0,     :null => false
    t.integer  "failed_login_count",    :default => 0,     :null => false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "sex"
    t.string   "place_of_birth"
    t.datetime "birth_date"
    t.string   "civil_state"
    t.string   "phone"
    t.string   "cell_phone"
    t.string   "rg"
    t.string   "rg_oe"
    t.string   "rg_ee"
    t.string   "cpf"
    t.string   "rne"
    t.string   "high_school"
    t.boolean  "ex_student"
    t.boolean  "special_needs"
    t.string   "special_needs_visual"
    t.string   "special_needs_phisics"
    t.string   "special_needs_hearing"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.boolean  "cms_user",              :default => false
  end

  add_index "users_2016", ["cpf"], :name => "idx_3"
  add_index "users_2016", ["email"], :name => "idx_2"
  add_index "users_2016", ["perishable_token", "updated_at"], :name => "idx_4"
  add_index "users_2016", ["persistence_token"], :name => "idx_1"

  create_table "users_2017", :force => true do |t|
    t.string   "name",                                     :null => false
    t.string   "last_name"
    t.string   "email",                                    :null => false
    t.string   "crypted_password",                         :null => false
    t.string   "password_salt",                            :null => false
    t.string   "persistence_token",                        :null => false
    t.string   "perishable_token",                         :null => false
    t.integer  "login_count",           :default => 0,     :null => false
    t.integer  "failed_login_count",    :default => 0,     :null => false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip"
    t.string   "last_login_ip"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "sex"
    t.string   "place_of_birth"
    t.datetime "birth_date"
    t.string   "civil_state"
    t.string   "phone"
    t.string   "cell_phone"
    t.string   "rg"
    t.string   "rg_oe"
    t.string   "rg_ee"
    t.string   "cpf"
    t.string   "rne"
    t.string   "high_school"
    t.boolean  "ex_student"
    t.boolean  "special_needs"
    t.string   "special_needs_visual"
    t.string   "special_needs_phisics"
    t.string   "special_needs_hearing"
    t.string   "address"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.string   "neighbourhood"
    t.string   "state"
    t.string   "city"
    t.integer  "exam_id"
    t.integer  "socioeconomic_id"
    t.integer  "first_option_id"
    t.integer  "second_option_id"
    t.boolean  "cms_user",              :default => false
  end

  add_index "users_2017", ["cpf"], :name => "idx_3"
  add_index "users_2017", ["email"], :name => "idx_2"
  add_index "users_2017", ["perishable_token", "updated_at"], :name => "idx_4"
  add_index "users_2017", ["persistence_token"], :name => "idx_1"

  create_table "users_2018", :force => true do |t|
    t.string    "name",                                                   :null => false
    t.string    "last_name"
    t.string    "email_copy1",                                            :null => false
    t.string    "crypted_password",                                       :null => false
    t.string    "password_salt",                                          :null => false
    t.string    "persistence_token",                                      :null => false
    t.string    "perishable_token",                                       :null => false
    t.integer   "login_count",                         :default => 0,     :null => false
    t.integer   "failed_login_count",                  :default => 0,     :null => false
    t.datetime  "last_request_at"
    t.datetime  "current_login_at"
    t.datetime  "last_login_at"
    t.string    "current_login_ip"
    t.string    "last_login_ip"
    t.datetime  "created_at",                                             :null => false
    t.datetime  "updated_at",                                             :null => false
    t.string    "sex"
    t.string    "place_of_birth"
    t.datetime  "birth_date"
    t.string    "civil_state"
    t.string    "phone"
    t.string    "cell_phone"
    t.string    "rg"
    t.string    "rg_oe"
    t.string    "rg_ee"
    t.string    "cpf"
    t.string    "rne"
    t.string    "high_school"
    t.boolean   "ex_student"
    t.boolean   "special_needs"
    t.string    "special_needs_visual"
    t.string    "special_needs_phisics"
    t.string    "special_needs_hearing"
    t.string    "address"
    t.string    "postal_code"
    t.integer   "number"
    t.string    "complement"
    t.string    "neighbourhood"
    t.string    "state"
    t.string    "city"
    t.integer   "exam_id"
    t.integer   "socioeconomic_id"
    t.integer   "first_option_id"
    t.integer   "second_option_id"
    t.boolean   "cms_user",                            :default => false
    t.integer   "nota_enem"
    t.string    "social_name"
    t.string    "username",              :limit => 16,                    :null => false
    t.string    "email"
    t.string    "password",              :limit => 32,                    :null => false
    t.timestamp "create_time"
  end

  create_table "warning_images", :force => true do |t|
    t.integer  "warning_id"
    t.string   "name"
    t.boolean  "active",                      :default => true, :null => false
    t.boolean  "published",                   :default => true, :null => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "warning_images_file_name"
    t.string   "warning_images_content_type"
    t.integer  "warning_images_file_size"
    t.datetime "warning_images_updated_at"
  end

  add_index "warning_images", ["warning_id"], :name => "idx_1"

  create_table "warnings", :force => true do |t|
    t.integer  "type_warning_id"
    t.string   "name"
    t.string   "short_description"
    t.text     "description"
    t.boolean  "active",                     :default => true,  :null => false
    t.boolean  "published",                  :default => true,  :null => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.boolean  "show_home",                  :default => false, :null => false
    t.string   "warning_image_file_name"
    t.string   "warning_image_content_type"
    t.integer  "warning_image_file_size"
    t.datetime "warning_image_updated_at"
    t.string   "warning_file_file_name"
    t.string   "warning_file_content_type"
    t.integer  "warning_file_file_size"
    t.datetime "warning_file_updated_at"
    t.boolean  "is_library",                 :default => false, :null => false
  end

  add_index "warnings", ["active", "description", "name", "published"], :name => "idx_3", :length => {"description"=>100, "active"=>nil, "name"=>nil, "published"=>nil}
  add_index "warnings", ["active", "published", "type_warning_id"], :name => "idx_1"
  add_index "warnings", ["type_warning_id"], :name => "idx_2"

  create_table "works", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.integer  "department_id"
    t.text     "message"
    t.boolean  "active",                       :default => true, :null => false
    t.boolean  "published",                    :default => true, :null => false
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.string   "attachment_work_file_name"
    t.string   "attachment_work_content_type"
    t.integer  "attachment_work_file_size"
    t.datetime "attachment_work_updated_at"
  end

end
