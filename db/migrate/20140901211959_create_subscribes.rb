class CreateSubscribes < ActiveRecord::Migration
  def self.up
    create_table :subscribes do |t|
      t.integer :exam_id
      t.integer :socioeconomic_id
      t.integer :user_id
      t.decimal :price
      t.string :ticket_number
      t.timestamps
    end
  end

  def self.down
    drop_table :subscribes
  end
end
