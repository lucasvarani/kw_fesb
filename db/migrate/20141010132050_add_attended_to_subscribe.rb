class AddAttendedToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :attended, :boolean,:null => false, :default => false
  end
end
