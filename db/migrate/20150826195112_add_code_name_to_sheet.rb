class AddCodeNameToSheet < ActiveRecord::Migration
  def change
    add_column :sheets, :code_name, :string
  end
end
