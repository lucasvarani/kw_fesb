class AddShowHomeToWarning < ActiveRecord::Migration
  def change
    add_column :warnings, :show_home, :boolean,:null => false, :default => 0
  end
end
