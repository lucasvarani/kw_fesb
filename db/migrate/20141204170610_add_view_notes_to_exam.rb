class AddViewNotesToExam < ActiveRecord::Migration
  def change
    add_column :exams, :view_notes, :boolean, :null => false, :default => false
  end
end
