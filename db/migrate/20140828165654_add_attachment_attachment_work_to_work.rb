class AddAttachmentAttachmentWorkToWork < ActiveRecord::Migration
  def self.up
    add_column :works, :attachment_work_file_name, :string
    add_column :works, :attachment_work_content_type, :string
    add_column :works, :attachment_work_file_size, :integer
    add_column :works, :attachment_work_updated_at, :datetime
  end

  def self.down
    remove_column :works, :attachment_work_file_name
    remove_column :works, :attachment_work_content_type
    remove_column :works, :attachment_work_file_size
    remove_column :works, :attachment_work_updated_at
  end
end
