class AddClassIdToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :first_option_id, :integer
    add_column :subscribes, :second_option_id, :integer
  end
end
