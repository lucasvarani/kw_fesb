class CreateExams < ActiveRecord::Migration
  def self.up
    create_table :exams do |t|
      t.string :name
      t.integer :exam_type_id
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :published, :default => 0
      t.boolean :active, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :exams
  end
end
