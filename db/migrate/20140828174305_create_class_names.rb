class CreateClassNames < ActiveRecord::Migration
  def self.up
    create_table :class_names do |t|
      t.integer :class_category_id
      t.string :name
      t.string :duration
      t.string :period
      t.string :number_of_vacancies
      t.text :description
      t.text :labour_market
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :class_names
  end
end
