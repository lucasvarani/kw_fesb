class CreateAwardGalleries < ActiveRecord::Migration
  def self.up
    create_table :award_galleries do |t|
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :award_galleries
  end
end
