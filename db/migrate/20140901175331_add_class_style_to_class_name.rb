class AddClassStyleToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :class_style_id, :integer
  end
end
