class AddScheduleDateEndToSchedule < ActiveRecord::Migration
  def change
    add_column :schedules, :schedule_date_end, :date
  end
end
