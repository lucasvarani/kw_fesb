class AddShowSubscribeToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :show_subscribe, :boolean, :null => false, :default => true
  end
end
