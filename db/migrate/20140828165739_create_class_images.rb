class CreateClassImages < ActiveRecord::Migration
  def self.up
    create_table :class_images do |t|
      t.integer :class_name_id
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :class_images
  end
end
