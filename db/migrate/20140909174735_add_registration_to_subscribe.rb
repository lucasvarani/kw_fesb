class AddRegistrationToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :registration, :string
  end
end
