class CreateWarnings < ActiveRecord::Migration
  def self.up
    create_table :warnings do |t|
      t.integer :type_warning_id
      t.string :name
      t.string :short_description
      t.text :description
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :warnings
  end
end
