class CreateFileImports < ActiveRecord::Migration
  def self.up
    create_table :file_imports do |t|
      t.text :log
      t.text :log
      t.text :error
      t.integer :user_id
      t.timestamps
    end
  end

  def self.down
    drop_table :file_imports
  end
end
