class AddSchedulingToRegistration < ActiveRecord::Migration
  def change
    add_column :registrations, :scheduling, :date
  end
end
