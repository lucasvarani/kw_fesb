class AddCmsUserToUser < ActiveRecord::Migration
  def change
    add_column :users, :cms_user, :boolean, :default => false
  end
end
