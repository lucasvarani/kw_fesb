class CreateEnems < ActiveRecord::Migration
  def change
    create_table :enems do |t|
      t.string :name
      t.string :cell
      t.string :cpf
      t.integer :nota
      t.string :curso

      t.timestamps
    end
  end
end
