class AddComplementNoteToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :copywriting_note, :decimal, :precision => 10, :scale => 4
    add_column :subscribes, :note_issues, :decimal, :precision => 10, :scale => 4
    add_column :subscribes, :copywriting_weight, :decimal, :precision => 10, :scale => 4
    add_column :subscribes, :weight_issues, :decimal, :precision => 10, :scale => 4
    add_column :subscribes, :endnote, :decimal, :precision => 10, :scale => 5
  end
end
