class CreateItemImages < ActiveRecord::Migration
  def self.up
    create_table :item_images do |t|
      t.integer :item_id
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :item_images
  end
end
