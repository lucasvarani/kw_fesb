class AddPreSubscribeToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :pre_subscribe, :boolean,:null => false, :default => false
  end
end
