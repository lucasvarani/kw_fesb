class AddExamIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :exam_id, :integer
    add_column :users, :socioeconomic_id, :integer
  end
end
