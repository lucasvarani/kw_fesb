class AddIsLibraryToWarning < ActiveRecord::Migration
  def change
    add_column :warnings, :is_library, :boolean,:null => false, :default => false
  end
end
