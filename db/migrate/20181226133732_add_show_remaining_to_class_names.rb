class AddShowRemainingToClassNames < ActiveRecord::Migration
  def change
    add_column :class_names, :show_remaining, :boolean, :null => false, :default => true
  end
end
