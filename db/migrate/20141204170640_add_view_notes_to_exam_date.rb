class AddViewNotesToExamDate < ActiveRecord::Migration
  def change
    add_column :exam_dates, :view_notes, :boolean, :null => false, :default => false
  end
end
