class AddClassNameIdToSheet < ActiveRecord::Migration
  def change
    add_column :sheets, :class_name_id, :integer
  end
end
