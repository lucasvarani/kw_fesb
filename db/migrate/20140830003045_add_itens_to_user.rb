class AddItensToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :sex, :string
    add_column :users, :place_of_birth, :string
    add_column :users, :birth_date, :datetime
    add_column :users, :civil_state, :string
    add_column :users, :phone, :string
    add_column :users, :cell_phone, :string
    add_column :users, :rg, :string
    add_column :users, :rg_oe, :string
    add_column :users, :rg_ee, :string
    add_column :users, :cpf, :string
    add_column :users, :rne, :string
    add_column :users, :high_school, :integer
    add_column :users, :ex_student, :boolean
    add_column :users, :special_needs, :boolean
    add_column :users, :special_needs_visual, :string
    add_column :users, :special_needs_phisics, :string
    add_column :users, :special_needs_hearing, :string
  end
  
  def self.down
    remove_column :users, :sex
    remove_column :users, :place_of_birth
    remove_column :users, :birth_date
    remove_column :users, :civil_state
    remove_column :users, :phone
    remove_column :users, :cell_phone
    remove_column :users, :rg
    remove_column :users, :rg_oe
    remove_column :users, :rg_ee
    remove_column :users, :cpf
    remove_column :users, :rne
    remove_column :users, :high_school
    remove_column :users, :ex_student
    remove_column :users, :special_needs
    remove_column :users, :special_needs_visual
    remove_column :users, :special_needs_phisics
    remove_column :users, :special_needs_hearing
  end
end
