class AddCpfToPreSubscribes < ActiveRecord::Migration
  def change
    add_column :pre_subscribes, :cpf, :string
  end
end
