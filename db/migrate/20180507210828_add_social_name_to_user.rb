class AddSocialNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :social_name, :string
  end
end
