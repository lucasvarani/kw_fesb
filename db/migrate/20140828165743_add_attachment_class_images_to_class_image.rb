class AddAttachmentClassImagesToClassImage < ActiveRecord::Migration
  def self.up
    add_column :class_images, :class_images_file_name, :string
    add_column :class_images, :class_images_content_type, :string
    add_column :class_images, :class_images_file_size, :integer
    add_column :class_images, :class_images_updated_at, :datetime
  end

  def self.down
    remove_column :class_images, :class_images_file_name
    remove_column :class_images, :class_images_content_type
    remove_column :class_images, :class_images_file_size
    remove_column :class_images, :class_images_updated_at
  end
end
