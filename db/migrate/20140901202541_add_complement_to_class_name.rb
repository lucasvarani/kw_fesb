class AddComplementToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :objective, :text
    add_column :class_names, :differential, :text
    add_column :class_names, :target, :text
    add_column :class_names, :requirements, :text
  end
end
