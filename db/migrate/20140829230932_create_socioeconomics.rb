class CreateSocioeconomics < ActiveRecord::Migration
  def self.up
    create_table :socioeconomics do |t|
      t.string :birth_state
      t.string :birth_city
      t.string :income
      t.string :income_family
      t.string :school
      t.string :highschool
      t.string :occupation
      t.string :how_did_me
      t.string :best_ad
      t.timestamps
    end
  end

  def self.down
    drop_table :socioeconomics
  end
end
