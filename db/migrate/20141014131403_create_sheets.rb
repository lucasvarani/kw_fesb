class CreateSheets < ActiveRecord::Migration
  def self.up
    create_table :sheets do |t|
      t.string :name
      t.string :email
      t.string :ra
      t.string :author
      t.string :advisor
      t.string :title_subtitle
      t.string :course
      t.integer :number_pages
      t.integer :number_illustrations
      t.string :keywords
      t.text :additional_information
      t.timestamps
    end
  end

  def self.down
    drop_table :sheets
  end
end
