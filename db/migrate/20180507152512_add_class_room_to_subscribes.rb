class AddClassRoomToSubscribes < ActiveRecord::Migration
  def change
    add_column :subscribes, :class_room, :string
  end
end
