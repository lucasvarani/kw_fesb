class CreateScientificInitiations < ActiveRecord::Migration
  def self.up
    create_table :scientific_initiations do |t|
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :scientific_initiations
  end
end
