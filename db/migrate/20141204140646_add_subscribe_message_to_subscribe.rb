class AddSubscribeMessageToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :subscribe_message, :string
  end
end
