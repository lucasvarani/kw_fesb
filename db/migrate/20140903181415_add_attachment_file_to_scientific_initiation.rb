class AddAttachmentFileToScientificInitiation < ActiveRecord::Migration
  def self.up
    add_column :scientific_initiations, :file_file_name, :string
    add_column :scientific_initiations, :file_content_type, :string
    add_column :scientific_initiations, :file_file_size, :integer
    add_column :scientific_initiations, :file_updated_at, :datetime
  end

  def self.down
    remove_column :scientific_initiations, :file_file_name
    remove_column :scientific_initiations, :file_content_type
    remove_column :scientific_initiations, :file_file_size
    remove_column :scientific_initiations, :file_updated_at
  end
end
