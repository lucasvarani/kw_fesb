class CreateExAlunos < ActiveRecord::Migration
  def self.up
    create_table :ex_alunos do |t|
      t.string :name
      t.string :email
      t.string :subject
      t.string :phone
      t.string :message
      t.string :curso
      t.string :conclusion_year
      t.timestamps
    end
  end

  def self.down
    drop_table :ex_alunos
  end
end
