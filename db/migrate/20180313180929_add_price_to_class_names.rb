class AddPriceToClassNames < ActiveRecord::Migration
  def change
    add_column :class_names, :price, :string
  end
end
