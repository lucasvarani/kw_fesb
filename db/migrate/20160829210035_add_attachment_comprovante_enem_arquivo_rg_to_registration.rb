class AddAttachmentComprovanteEnemArquivoRgToRegistration < ActiveRecord::Migration
  def self.up
    add_column :registrations, :comprovante_enem_file_name, :string
    add_column :registrations, :comprovante_enem_content_type, :string
    add_column :registrations, :comprovante_enem_file_size, :integer
    add_column :registrations, :comprovante_enem_updated_at, :datetime
    add_column :registrations, :arquivo_rg_file_name, :string
    add_column :registrations, :arquivo_rg_content_type, :string
    add_column :registrations, :arquivo_rg_file_size, :integer
    add_column :registrations, :arquivo_rg_updated_at, :datetime
  end

  def self.down
    remove_column :registrations, :comprovante_enem_file_name
    remove_column :registrations, :comprovante_enem_content_type
    remove_column :registrations, :comprovante_enem_file_size
    remove_column :registrations, :comprovante_enem_updated_at
    remove_column :registrations, :arquivo_rg_file_name
    remove_column :registrations, :arquivo_rg_content_type
    remove_column :registrations, :arquivo_rg_file_size
    remove_column :registrations, :arquivo_rg_updated_at
  end
end
