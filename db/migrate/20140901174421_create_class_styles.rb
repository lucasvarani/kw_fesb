class CreateClassStyles < ActiveRecord::Migration
  def self.up
    create_table :class_styles do |t|
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :class_styles
  end
end
