class AddAttachmentClassHeaderToClassName < ActiveRecord::Migration
  def self.up
    add_column :class_names, :class_header_file_name, :string
    add_column :class_names, :class_header_content_type, :string
    add_column :class_names, :class_header_file_size, :integer
    add_column :class_names, :class_header_updated_at, :datetime
  end

  def self.down
    remove_column :class_names, :class_header_file_name
    remove_column :class_names, :class_header_content_type
    remove_column :class_names, :class_header_file_size
    remove_column :class_names, :class_header_updated_at
  end
end
