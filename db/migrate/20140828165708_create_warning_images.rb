class CreateWarningImages < ActiveRecord::Migration
  def self.up
    create_table :warning_images do |t|
      t.integer :warning_id
      t.string :name
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :warning_images
  end
end
