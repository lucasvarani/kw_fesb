class AddShowItemToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :show_item, :boolean,:null => false, :default => 0 
  end
end
