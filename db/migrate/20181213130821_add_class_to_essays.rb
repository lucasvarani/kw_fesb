class AddClassToEssays < ActiveRecord::Migration
  def change
    add_column :essays, :class, :string
  end
end
