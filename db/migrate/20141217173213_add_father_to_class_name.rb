class AddFatherToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :father_id, :integer
  end
end
