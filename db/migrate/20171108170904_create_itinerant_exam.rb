class CreateItinerantExam < ActiveRecord::Migration
  def self.up
    create_table :itinerant_exams do |t|
      t.string :city
      t.string :phone
      t.string :address
      t.timestamps
    end

  end

  def self.down
    drop_table :itinerant_exams
  end
end
