class CreatePreSubscribes < ActiveRecord::Migration
  def self.up
    create_table :pre_subscribes do |t|
      t.integer :class_name_id
      t.string :name
      t.string :email
      t.string :phone
      t.text :message
      t.boolean :active,:null => false, :default => 1 
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :pre_subscribes
  end
end
