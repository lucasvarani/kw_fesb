class AddYearToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :year, :integer, :default => "2015"
  end
end
