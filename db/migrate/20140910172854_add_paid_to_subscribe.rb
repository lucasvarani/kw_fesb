class AddPaidToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :paid, :boolean, :null => false, :default => 0
  end
end
