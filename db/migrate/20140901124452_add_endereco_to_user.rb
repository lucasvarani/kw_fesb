class AddEnderecoToUser < ActiveRecord::Migration
  def change
    add_column :users, :address, :string
    add_column :users, :postal_code, :string
    add_column :users, :number, :integer
    add_column :users, :complement, :string
    add_column :users, :neighbourhood, :string
    add_column :users, :state, :string
    add_column :users, :city, :string
  end
end
