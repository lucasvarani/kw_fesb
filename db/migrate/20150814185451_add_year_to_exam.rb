class AddYearToExam < ActiveRecord::Migration
  def change
    add_column :exams, :year, :integer, :default => "2015"
  end
end
