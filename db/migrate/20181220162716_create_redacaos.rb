class CreateRedacaos < ActiveRecord::Migration
  def change
    create_table :redacaos do |t|
      t.string :name
      t.string :email
      t.string :address
      t.string :cpf
      t.string :cell
      t.text :essay

      t.timestamps
    end
  end
end
