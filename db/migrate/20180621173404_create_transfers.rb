class CreateTransfers < ActiveRecord::Migration
  def self.up
    create_table :transfers do |t|
      t.string :name
      t.string :email
      t.string :city
      t.string :phone
      t.string :curso
      t.timestamps
    end
  end
end
