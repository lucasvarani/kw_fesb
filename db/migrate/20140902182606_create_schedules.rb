class CreateSchedules < ActiveRecord::Migration
  def self.up
    create_table :schedules do |t|
      t.string :name
      t.date :schedule_date
      t.time :schedule_time
      t.string :description
      t.boolean :published,:null => false, :default => 1
      t.boolean :active,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :schedules
  end
end
