class AddAttachmentAwardImgToAwardGallery < ActiveRecord::Migration
  def self.up
    add_column :award_galleries, :award_img_file_name, :string
    add_column :award_galleries, :award_img_content_type, :string
    add_column :award_galleries, :award_img_file_size, :integer
    add_column :award_galleries, :award_img_updated_at, :datetime
  end

  def self.down
    remove_column :award_galleries, :award_img_file_name
    remove_column :award_galleries, :award_img_content_type
    remove_column :award_galleries, :award_img_file_size
    remove_column :award_galleries, :award_img_updated_at
  end
end
