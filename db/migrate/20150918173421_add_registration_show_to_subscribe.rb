class AddRegistrationShowToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :registrationshow, :boolean,:null => false, :default => 0
  end
end
