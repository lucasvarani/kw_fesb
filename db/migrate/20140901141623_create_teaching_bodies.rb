class CreateTeachingBodies < ActiveRecord::Migration
  def change
    create_table :teaching_bodies do |t|
      t.references :class_name, :teacher 
      t.boolean :coordinator,:null => false, :default => false 
      t.timestamps
    end
  end
end
