class AddSocialNameToPreSubscribe < ActiveRecord::Migration
  def change
    add_column :pre_subscribes, :social_name, :string
  end
end
