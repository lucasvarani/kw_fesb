class AddAttachmentAttachToSchedule < ActiveRecord::Migration
  def self.up
    add_column :schedules, :attach_file_name, :string
    add_column :schedules, :attach_content_type, :string
    add_column :schedules, :attach_file_size, :integer
    add_column :schedules, :attach_updated_at, :datetime
  end

  def self.down
    remove_column :schedules, :attach_file_name
    remove_column :schedules, :attach_content_type
    remove_column :schedules, :attach_file_size
    remove_column :schedules, :attach_updated_at
  end
end
