class AddEmbedToClassName < ActiveRecord::Migration
  def change
    add_column :class_names, :embed, :string
  end
end
