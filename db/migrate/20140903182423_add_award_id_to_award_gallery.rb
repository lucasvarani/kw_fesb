class AddAwardIdToAwardGallery < ActiveRecord::Migration
  def change
    add_column :award_galleries, :award_id, :integer
  end
end
