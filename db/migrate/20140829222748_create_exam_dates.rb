class CreateExamDates < ActiveRecord::Migration
  def self.up
    create_table :exam_dates do |t|
      t.datetime :exam_date
      t.integer :exam_id
      t.boolean :published, :default => 1
      t.boolean :active, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :exam_dates
  end
end
