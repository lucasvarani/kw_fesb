class AddOptionsToUser < ActiveRecord::Migration
  def change
    add_column :users, :first_option_id, :integer
    add_column :users, :second_option_id, :integer
  end
end
