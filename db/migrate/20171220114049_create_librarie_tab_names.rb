class CreateLibrarieTabNames < ActiveRecord::Migration
  def self.up
    create_table :librarie_tab_names do |t|
      t.string :name
    end
  end

  def self.down
    drop_table :librarie_tab_names
  end
end
