class AddAttachmentWarningImagesToWarningImage < ActiveRecord::Migration
  def self.up
    add_column :warning_images, :warning_images_file_name, :string
    add_column :warning_images, :warning_images_content_type, :string
    add_column :warning_images, :warning_images_file_size, :integer
    add_column :warning_images, :warning_images_updated_at, :datetime
  end

  def self.down
    remove_column :warning_images, :warning_images_file_name
    remove_column :warning_images, :warning_images_content_type
    remove_column :warning_images, :warning_images_file_size
    remove_column :warning_images, :warning_images_updated_at
  end
end
