class AddTicketLinkToSubscribe < ActiveRecord::Migration
  def change
    add_column :subscribes, :ticket_link, :text
  end
end
