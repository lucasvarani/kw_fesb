module ApplicationHelper

  def number_to_currency_br(number)
    number_to_currency(number, :unit => "R$ ", :separator => ",", :delimiter => ".")
  end
  
  def if_hightarget(value)
    if value == "_blank"
      return "Nova aba"
    else
      return "Mesma aba"
    end
  end

  def if_highflag(value)
    if value == 1
      return "Cabeçalho"
    else
      return "Corpo"
    end
  end

  def show_type_color(value)
    case value
      when 1 then return "badge-primary"
      when 2 then return "badge-danger"
      when 3 then return "badge-success"
      when 4 then return "badge-warning"
      when 5 then return "badge-primary"
      when 6 then return "badge-danger"
      when 7 then return "badge-success"
    end
  end

  def pagination_links(collection, options = {})
     options[:renderer] ||= BootstrapPaginationHelper::LinkRenderer
     options[:class] ||= 'pagination pagination-centered'
     options[:inner_window] ||= 2
     options[:outer_window] ||= 1
     options[:previous_label] ||= "anterior"
     options[:next_label] ||= "próximo"
     options[:page_gap] ||= "&hellip;"
     will_paginate(collection, options)
   end

   def show_boolean_special_needs(value)
     case value
       when 0 then return "Não"
       when 1 then return "Sim"
     end
   end

  def show_boolean(value)
    if value
      return "Sim"
    else
      return "Não"
    end
  end

  def show_age(age)
    case age
      when 1 then return "0 - 2"
      when 2 then return "3 +"
    end
  end

  def language_links
      links = []
      I18n.available_locales.each do |locale|
        locale_key = "translation.#{locale}"
        if locale == I18n.locale
          links << link_to(image_tag("icon-flag-#{locale.to_s}-off.png"), "#", :class=> "btn_language disabled")
        else
          links << link_to(image_tag("icon-flag-#{locale.to_s}.png"), url_for(:locale => locale.to_s), :class=> "btn_language")
        end
      end
      links.join("\n").html_safe
  end

  def tr_ensino(value)
    case value.to_i
        when 0 then return "Escola Pública"
        when 1 then return "Escola Particular"
    end
  end

  def salarios(value)
    case value.to_i
        when 1 then return "Até 1 Salário Mínimo"
        when 2 then return "De 2 a 4 Salários Mínimos"
        when 3 then return "De 5 a 7 Salários Mínimos"
        when 4 then return "Acima de 8 Salários Mínimos"
    end
  end

  def salarios_familia(value)
    case value.to_i
        when 0 then return "De 2 a 3 salários mínimos"
        when 1 then return "De 4 a 6 salários mínimos"
        when 2 then return "De 7 a 10 salários mínimos"
    end
  end

  def conheceu(value)
    case value.to_i
        when 1 then return "Indicação de Amigos"
        when 2 then return "Campanha Publicitária"
        when 4 then return "Internet"
    end
  end

  def melhor_anuncio(value)
    case value.to_i
        when 2 then return "Rádio"
        when 3 then return "Site"
        when 4 then return "Outdoor"
        when 8 then return "TV"
        when 9 then return "Visita as Escolas"
        when 10 then return "Facebook"  
        when 11 then return "Anúncio da internet"
    end
  end

  def inverte_sobrenome(name)
    palavras = name.split(" ")
    inverte = palavras.last+","
    i = 0
    while i < palavras.size - 1
      inverte += " "+palavras[i]
      i += 1
    end
    return inverte
  end

  def show_genero(genero)
    case genero
      when "F" then return "Feminino"
      when "M" then return "Masculino"
    else
      return "Erro"
    end
  end

  def show_civil_state(civil_state)
    case civil_state
      when "C" then return "Casado"
      when "D" then return "Divorciado"
      when "S" then return "Solteiro"
      when "V" then return "Viúvo"
    else
      return "Erro"
    end
  end

  def name_length(tab)
    if tab > 15
      return "py-custom-1"
    else
      return "py-5"
    end
  end
end
