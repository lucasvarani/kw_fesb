class Notifier < ActionMailer::Base
  helper :application
  default :from => "Fesb <no-reply@sato7.com.br>"

  def send_contact_user(contact)
    @sitecontact = contact
    mail(:to => @sitecontact.email, :bcc => "log@korewa.com.br", :subject => "Contato enviado - Protocolo Nº#{@sitecontact.id} ")
  end

  def send_contact(contact, to)
    @sitecontact = contact
    mail(:to => to, :bcc => "log@korewa.com.br", :subject => "Novo contato pelo site - Protocolo Nº#{@sitecontact.id} ")
  end

  def send_contact_user_exaluno(ex_aluno)
    @ex_aluno = ex_aluno
    mail(:to => @ex_aluno.email, :bcc => "log@korewa.com.br", :subject => "Contato enviado - Protocolo Nº#{@ex_aluno.id} ")
  end

  def send_contact_exaluno(ex_aluno)
    @ex_aluno = ex_aluno
    mail(:to => "priscila@fesb.edu.br", :bcc => "log@korewa.com.br", :subject => "Novo contato de Ex Aluno pelo site - Protocolo Nº#{@ex_aluno.id} ")
  end

  def send_transfer_user(transfer)
    @transfer = transfer
    mail(:to => @transfer.email, :bcc => "log@korewa.com.br", :subject => "Transferência enviada - Protocolo Nº#{@transfer.id} ")
  end

  def send_transfer(transfer)
    @transfer = transfer
    mail(:to => "faculdade@fesb.edu.br", :bcc => "a.nascimento@sato7.com.br", :subject => "Nova Transferência pelo site - Protocolo Nº#{@transfer.id} ")
  end

  def send_work_user(work)
    @work = work
    mail(:to => @work.email, :bcc => "log@korewa.com.br", :subject => "Currículo enviado - Protocolo Nº#{@work.id} ")
  end

  def send_error(error)
    @error = error
    mail(:to => "log@korewa.com.br", :subject => "[FESB] Erro na aplicação")
  end

  def send_work(work)
    @work = work
    mail(:to => "rh@fesb.edu.br", :bcc => "log@korewa.com.br", :subject => "Novo currículo enviado pelo site - Protocolo Nº#{@work.id} ")
  end

  def send_paid_confirmation (subscribe)
    @subscribe=subscribe
    mail(:to => subscribe.user.email, :bcc => "log@korewa.com.br", :subject => "Confirmação de Pagamento. Inscrição Número #{subscribe.registration}")
  end

#Formulário de pré-inscrição
  def send_pre_subscribe_user(pre_subscribe, class_name)
    @pre_subscribe = pre_subscribe
    @class_name = class_name
    mail(:to => @pre_subscribe.email, :bcc => "log@korewa.com.br", :subject => "Pré-Matrícula enviada - Protocolo Nº#{@pre_subscribe.id} ")
  end

  def send_pre_subscribe_clinic(pre_subscribe)
    @pre_subscribe = pre_subscribe
    mail(:to => "nutricaoclinica@fesb.edu.br", :bcc => "marketing@fesb.edu.br", :subject => "Nova Pré-Inscrição enviada pelo site - Protocolo Nº#{@pre_subscribe.id} ")
  end

  def send_pre_subscribe(pre_subscribe)
    @pre_subscribe = pre_subscribe
    mail(:to => "pos@fesb.edu.br", :bcc => "adm@pos.edu.br", :subject => "Nova Pré-Matrícula enviada pelo site - Protocolo Nº#{@pre_subscribe.id} ")
  end

  def send_subscribe_user(user, subscribe)
    @user = user
    @subscribe = subscribe
    mail(:to => user.email, :bcc => "log@korewa.com.br", :subject => "Inscrição efetuada com sucesso! ")
  end

  def send_subscribe(subscribe)
    @subscribe = subscribe
    mail(:to => "aangelis@sato7.com.br", :bcc => "log@korewa.com.br", :subject => "Nova Inscrição enviada pelo site - Protocolo Nº#{@subscribe.id} ")
  end
#fim Formulário de pré-inscrição

  def send_email_password_reset(user)
    @user = user
		mail(:to => user.email, :bcc => "log@korewa.com.br", :subject => "FESB - Instruções para trocar a senha")
	end

	def send_sheet_to_user(sheet)
	  @sheet = sheet
	  mail(:to => @sheet.email, :bcc => "log@korewa.com.br", :subject => "FESB - Ficha Catalográfica")
	end

	def send_sheet_to_admin(sheet)
	  @sheet = sheet
	  mail(:to =>"reinaldo@fesb.edu.br", :bcc => "log@korewa.com.br", :subject => "FESB - Ficha Catalográfica")
	end

	def send_registration(registration)
	  @registration = registration

	  if @registration.subscribe.ticket_link.present?
	    subject = "FESB - Cadastro de Pré Matricula"
      mail(:to =>"faculdade@fesb.edu.br", :bcc => "log@korewa.com.br", :subject => subject)
	  else
      subject = "FESB - Cadastro de Pré Matricula ENEM"
      mail(:to =>"priscila@fesb.edu.br", :bcc => "log@korewa.com.br", :subject => subject)
    end
	end

  def send_essay_user(redacao)
    @redacao = redacao
    mail(:to => redacao.email, :bcc => "log@korewa.com.br", :subject => "FESB - Parabéns! Você foi aprovado(a) na primeira fase!!")
  end

  def send_essay(redacao)
    @redacao = redacao
    mail(:to =>"marketing@fesb.edu.br ", :bcc => "raqueloriani@gmail.com", :cc => "jussara@fesb.edu.br", :subject => "FESB - vestibular inverno - Nova Redação")
  end

  def send_enem_user(enem)
    @enem = enem
    mail(:to => enem.email, :bcc => "log@korewa.com.br", :subject => "FESB - Parabéns, sua nota foi aprovada! Confira a documentação necessária para efetivação da matrícula!")
  end

  def send_enem(enem)
    @enem = enem
    mail(:to =>"olinda@fesb.edu.br", :bcc => "raqueloriani@gmail.com", :subject => "FESB - ENEM - Nova Inscrição")
  end

end
