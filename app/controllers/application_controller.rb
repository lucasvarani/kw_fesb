class ApplicationController < ActionController::Base
  helper :all
  protect_from_forgery
  helper_method :current_user
  # include SimpleCaptcha::ControllerHelpers

  #before_filter :set_locale
  #before_filter :set_locale_from_url

  def load_class
    @class_categories = ClassCategory.find(:all, :conditions=>["published = true and active = true"], :include => :class_names)
    @category3 = ClassCategory.find_by_id(3,:conditions=>["published = true and active = true"], :include => :class_names)
    @exams = Exam.all(:conditions => ["published = 1 AND active = 1 AND start_date < ? AND end_date > ?", DateTime.now, DateTime.now])
    @view_notes = ExamDate.all(:joins => "INNER JOIN `exams` ON `exam_dates`.`exam_id` = `exams`.`id`", :conditions => ["IF (`exams`.`exam_type_id` = 1, `exams`.`view_notes` = true,  `exams`.`view_notes` = true and `exam_dates`.`view_notes` = true)"])
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true'])
  end


  def gerar_numero(insc, tipo)
    if tipo.to_i == 1
      cod = 18
    else
      if
        tipo.to_i == 2
        cod = 17
      end
    end
    date = CURRENT_YEAR_EXAM
    numero = ''
    (5 - insc.id.to_s.length).times do |x|
      numero = numero + 0.to_s
    end
    show = date.to_s + cod.to_s + numero.to_s + insc.id.to_s
    numero = ''
    (15 - show.to_s.length).times do |x|
      numero = numero + 0.to_s
    end
    show = numero.to_s + show.to_s
    return show
  end




  def check_exam
    if session[:exam_id].nil?
      redirect_to exams_path
    end
  end

  def check_socioeconomic
    if session[:socioeconomic_id].nil?
      redirect_to exams_path
    end
  end

  def update_ticket(id, date)
    @subscribe = Subscribe.find(id)
    $params = "pkce=671&hsin=c4243b8f7e79bf0fd9c907f930dc8d6115ab5c30"
    $params += "&nome=#{@subscribe.user.name}"
    $params += "&insc=#{@subscribe.user.cpf}"
    $params += "&tiin=1"
    $params += "&nosn=#{@subscribe.ticket_number}"
    $params += "&numd=#{@subscribe.ticket_number.to_i.to_s }"
    $params += "&dtvc=#{(DateTime.now + date.to_i.days).strftime('%d%m%Y')}"
    $params += "&dtem=#{DateTime.now.strftime('%d%m%Y')}"
    $params += "&vlnm=#{@subscribe.price}"
    $params += "&cdmo=09&cddb=2&cdbd=1&cdpr=3&cdjr=1&cdes=18&cdde=1&cdfc=2&cdca=1";
    $params += "&msg1=Pagamento de Inscricao de #{@subscribe.exam.name} - www.fesb.br&msg2=Nao receber apos o vencimento";
    $params += "&msg3=Numero da Inscricao: #{@subscribe.registration}"
    $params += "&msg4=#{@subscribe.first_option_class.name.parameterize.titleize}&msg5="
    @boleto = 'https://secure1.nexxera.com/cobranca/sacado/criarBoleto.do?'+$params
    @subscribe.ticket_link = @boleto
    @subscribe.save
    return @subscribe.ticket_link
  end


  def set_header(p_type, filename)
    case p_type
      when 'xls'
       headers['Content-Type'] = "application/vnd.ms-excel; charset=UTF-8'"
       headers['Content-Disposition'] = "attachment; filename=\"#{filename}\""
       headers['Cache-Control'] = ''

      when 'msword'
       headers['Content-Type'] = "application/vnd.ms-word; charset=UTF-8"
       headers['Content-Disposition'] = "attachment; filename=\"#{filename}\""
       headers['Cache-Control'] = ''

     end
   end


  private

    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end

    rescue_from 'Acl9::AccessDenied', :with => :access_denied

    def access_denied
      if current_user
        redirect_to login_path, :notice => "Você não tem permissão para acessar está página!"
      else
        redirect_to login_path, :notice => "Acesso negado. Você precisa estar logado."
      end
    end

    def set_locale
      if lang = request.env['HTTP_ACCEPT_LANGUAGE']
        lang = lang[/^[a-z]{2}/]
        lang = :"pt-BR" if lang == "br"
      end
      I18n.locale = params[:locale] || lang || I18n.default_locale
    end



end
