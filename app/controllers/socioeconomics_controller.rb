class SocioeconomicsController < ApplicationController
  before_filter :load_class

  layout 'exams'

  def index

  end

  def new

    if !params[:exam_id].empty?
      #cookies[:exam_id] = { :value => params[:exam_id], :expires => Time.now + 7200}
      session[:exam_id] = params[:exam_id]
      @exam = Exam.find(session[:exam_id])

      if @exam.exam_type_id != 1 && @exam.exam_type_id != 3 && @exam.exam_type_id != 4
        session[:exam_date_id] = params[:exam_date_id]
      else
        session[:exam_date_id] = 0
      end
        @socioeconomic = Socioeconomic.new
    else
      redirect_to exams_path
    end
  end

  def create
    @socioeconomic = Socioeconomic.new(params[:socioeconomic])
    if @socioeconomic.save
      session[:socioeconomic_id] = @socioeconomic.id
      redirect_to new_user_path(:exam_id => session[:exam_id])
    else
      render :action => 'new'
    end
  end

  def update_cities
    city = City.find(params[:id])
    @cities = City.all(:conditions => ["state = ?", city.state]).map{|s| [s.title, s.id]}.insert(0, "Escolha uma cidade")
  end

  # def load_dropdown_list
  #   @states = City.all(:group => "state")
  # end
end
