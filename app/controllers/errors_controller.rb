class ErrorsController < ApplicationController
  def error_404
    render :file => "#{Rails.root}/public/404.html", :layout => false, :status => 404
  end
  
  def error_500
    Notifier.send_error("Ambiente: #{env["action_dispatch.env"]}- #{env["action_dispatch.exception"]} - Linha: #{env["action_dispatch.line_number"]} - Arquivo: #{env["action_dispatch.file"]}").deliver 
    render :file => "#{Rails.root}/public/500.html", :layout => false, :status => 500
  end
end
