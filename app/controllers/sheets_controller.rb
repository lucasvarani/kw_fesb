class SheetsController < ApplicationController
  before_filter :load_class, :load_cdd
  def new
    @sheet = Sheet.new
  end

  def create
    @sheet = Sheet.new(params[:sheet])
    if @sheet.save
      Notifier.send_sheet_to_user(@sheet).deliver
      Notifier.send_sheet_to_admin(@sheet).deliver
      redirect_to libraries_path, :notice => "Successfully created sheet."
    else
      render :action => 'new'
    end
  end

  def show
    @sheet = Sheet.find(params[:id])
  end

  def load_cdd
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @virtual_library = Category.find_by_id(10, :include => :items)
    @news_library = Category.find_by_id(12, :include => :items)
    @tcc_nt = Category.find_by_id(13, :include => :items)
    @sites_interessantes = Category.find_by_id(14, :include => :items)
    @class_names = ClassName.all(:conditions => ["published =  true and active = true and not cdd is null"]).collect {|c| [c.name, c.id]}
  end
end
