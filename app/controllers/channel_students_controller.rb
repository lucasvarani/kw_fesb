class ChannelStudentsController < ApplicationController
  before_filter :load_class
  def index
    @category = Category.find_by_id(5, :conditions => ['published =  true and active = true'])      
    if @category.blank?
      redirect_to root_path
    end
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true and id !=7 ']) 
    @warnings = Warning.paginate(:page => params[:page], :per_page => 5).find(:all, :conditions => ['published =  true and active = true and type_warning_id != 7'], :order => "id desc") 
  end

  def show  
    @category = Category.find_by_id(5, :conditions => ['published =  true and active = true'])      
    if @category.blank?
      redirect_to root_path
    end
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true']) 
    @warnings = Warning.paginate(:page => params[:page], :per_page => 5).all( :conditions => ['published =  true and active = true and type_warning_id = ?', params[:id]], :order => "id desc") 
    if !(@warnings.count > 0)
      redirect_to channel_students_path
    else
      render :index
    end
  end
end
