class RecipesController < ApplicationController

	def index
		@recipes = Recipe.all
	end

  def show 
    @recipe = Recipe.find(params[:id])
    if @recipe.blank?
      redirect_to root_path
    end    
  end

end
