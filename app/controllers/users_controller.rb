class UsersController < ApplicationController
  before_filter :load_class, :check_exam, :load_dropdown_lists
  require 'net/http'

  layout 'exams'

  def index
    redirect_to new_user_path
  end

  def new
    @exam_id = params[:exam_id]
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    if !current_user
      @user = User.new(:special_needs_visual => "Nao", :special_needs_phisics => "Nao", :special_needs_hearing => "Nao")
    else
      @user = User.find_by_id(current_user.id)
    end
  end

  def create
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    @user = User.new(params[:user])
    @user.role = "user"
    @user.cms_user = true
    if @user.save
      redirect_to user_path(@user), :notice => "Successfully created user."
    else
      if !@user.first_option_id.blank?
        class_name = ClassName.find(@user.first_option_id)
        @second = ClassName.all(:conditions => ["class_style_id = ? and not id = ?", class_name.class_style_id, class_name.id])
      else
        @second = ""
      end
      render :action => 'new'
    end
  end

  def payment
    @user = current_user
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    #SALVA INSCRICAO
    puts session[:exam_id]
    puts session[:socioeconomic_id]

    @subscribe = Subscribe.first(:conditions => ["exam_id = ? AND socioeconomic_id = ?", session[:exam_id], session[:socioeconomic_id]])
    if !@subscribe
      @subscribe = Subscribe.new
      @subscribe.exam_id = session[:exam_id]
      @subscribe.exam_date_id = session[:exam_date_id]
      @subscribe.socioeconomic_id = session[:socioeconomic_id]
      @subscribe.user_id = current_user.id
      @subscribe.save
      @subscribe.registration = gerar_numero(@subscribe, @subscribe.exam.exam_type_id).to_i.to_s
      @subscribe.ticket_number = "24"+gerar_numero(@subscribe, @subscribe.exam.exam_type_id).to_s
      @subscribe.price = 3000
      @subscribe.first_option_id = current_user.first_option_id
      @subscribe.second_option_id = current_user.second_option_id
      if @subscribe.exam.exam_type_id != 1
        time_difference = (@subscribe.exam_date.exam_date.to_date - Date.today).to_i - 1
      else
        time_difference = (@subscribe.exam.end_date.to_date - Date.today).to_i
      end
      puts "Diferen? de datas: " + time_difference.to_s
      time_difference = 0 if (time_difference < 0)
      if (time_difference >= 5)
        expiration_day = 5
      else
        expiration_day = time_difference
      end
      $params = "pkce=671&hsin=c4243b8f7e79bf0fd9c907f930dc8d6115ab5c30"
      $params += "&nome=#{current_user.name}"
      $params += "&insc=#{current_user.cpf}"
      $params += "&tiin=1"
      $params += "&nosn=#{@subscribe.ticket_number}"
      $params += "&numd=#{@subscribe.ticket_number.to_i.to_s }"
      $params += "&dtvc=#{(DateTime.now + expiration_day.days).strftime('%d%m%Y')}"
      $params += "&dtem=#{DateTime.now.strftime('%d%m%Y')}"
      $params += "&vlnm=#{@subscribe.price}"
      $params += "&cdmo=09&cddb=2&cdbd=1&cdpr=3&cdjr=1&cdes=18&cdde=1&cdfc=2&cdca=1";
      $params += "&msg1=Pagamento de Inscricao de #{@subscribe.exam.name} - www.fesb.br&msg2=Nao receber apos o vencimento";
      $params += "&msg3=Numero da Inscricao: #{@subscribe.registration}"
      $params += "&msg4=#{@subscribe.user.first_option_class.name.parameterize.titleize}&msg5="

      @boleto = 'https://secure1.nexxera.com/cobranca/sacado/criarBoleto.do?'+$params
      @subscribe.ticket_link = @boleto

      @subscribe.save
      @subscribe.year = @subscribe.exam.year
      @subscribe.save
      #ENVIAR EMAIL
      Notifier.send_subscribe_user(current_user, @subscribe).deliver
      Notifier.send_subscribe(@subscribe).deliver
    end

  end

  def update_second_option
    # updates songs based on artist selected
    @class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
    class_name = ClassName.find(params[:class_name_id])
    @second = ClassName.all(:conditions => ["published = 1 and active = 1 and show_subscribe= 1 and class_style_id = ? and not id = ? and id not in (?) and pre_subscribe = 0", class_name.class_style_id, class_name.id,@class_children.map{|s| s.father_id}], :order => :name).map{|s| [s.name, s.id]}.prepend(["Escolha a segunda op?o", nil])
  end

  def edit
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    if @exam
      @exam_dates = ExamDate.all(:conditions => ["exam_id = ? AND published = 1", @exam.id]).map{|s| [["Data: ", I18n.localize(s.exam_date, :format=>:short)," - " , "Cidade: ", s.itinerant_exam.city], s.id]}
    end
    @user = current_user
  end

  def show
    @user = current_user
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    @exam = Exam.find(session[:exam_id].to_i)
    @exam_type = @exam.exam_type_id
    #SE FOR ENEM OU SIMULADO, FAZER A INSCRICAO AGORA
    if @exam.exam_type_id == 3 || @exam.exam_type_id == 4
      @subscribe = Subscribe.first(:conditions => ["exam_id = ? AND socioeconomic_id = ?", session[:exam_id], session[:socioeconomic_id]])
      if @exam.exam_type_id == 3 and !@user.nota_enem
        redirect_to edit_user_path(@user), :notice => "Precisa inserir nota do ENEM"
      else
        if !@subscribe
          @subscribe = Subscribe.new
          if @exam.exam_type_id == 4
            @subscribe.paid = true
          end
          @subscribe.exam_id = session[:exam_id]
          @subscribe.exam_date_id = session[:exam_date_id]
          @subscribe.socioeconomic_id = session[:socioeconomic_id]
          @subscribe.user_id = current_user.id
          @subscribe.save
          @subscribe.registration = gerar_numero(@subscribe, @subscribe.exam.exam_type_id).to_i.to_s
          @subscribe.ticket_number = "0"
          @subscribe.price = 0000
          @subscribe.first_option_id = current_user.first_option_id
          @subscribe.second_option_id = current_user.second_option_id
          @subscribe.save
          @subscribe.year = @subscribe.exam.year
          @subscribe.save
        end
      end

    end



  end

  def update
    @exam_enem = Exam.find_by_id(session[:exam_id].to_i)
    @user = User.find(current_user.id)
    @user.cms_user = true
    if @user.update_attributes(params[:user])
      redirect_to user_path(@user), :notice  => "Successfully updated user."
    else
      render :action => 'edit'
    end
  end

  def load_dropdown_lists
    @class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
    @class_names = ClassName.all(:conditions => ["published = 1 and active = 1 and class_category_id = 1 and show_subscribe = 1 and id not in (?) and pre_subscribe = 0", @class_children.map{|s| s.father_id}], :order => :name)
    @second = ""
  end

  def search_cep
    url = URI::escape("http://webservice.kinghost.net/web_cep.php?auth=3d25c66b26b656be598f026367f47e8b&formato=json&cep=#{params[:cep]}")
    @return = Net::HTTP.get(URI.parse(url))
    render :json => @return
  end

end
