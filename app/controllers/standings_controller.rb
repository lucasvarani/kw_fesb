class StandingsController < ApplicationController
  before_filter :load_class
  def index
    @category = Category.find_by_id(5, :conditions => ['published =  true and active = true'])      
    if @category.blank?
      redirect_to root_path
    end
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true and id = 16']) 
    @warnings = Warning.paginate(:page => params[:page], :per_page => 5).find(:all, :conditions => ['published =  true and active = true and type_warning_id = 16'], :order => "id desc") 
  end

  def show  
    @category = Category.find_by_id(5, :conditions => ['published =  true and active = true'])      
    
    if @category.blank?
      redirect_to root_path
    end
    
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true and id = 16'])
    
    @warning = Warning.find_by_id(params[:id], :conditions => ['published =  true and active = true and type_warning_id = 16'])
    
  end
end
