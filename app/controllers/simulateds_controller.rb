class SimulatedsController < ApplicationController
  layout 'home'

  def index
  end

  def confirmation
    @user = User.find_by_cpf(params[:cpf])
    if !@user.blank?
      check_subscribes
    else
      flash[:notice]  = "CPF inválido tente novamente."
      redirect_to simulateds_path
    end
  end

  def check_subscribes
    @subscribe = @user.subscribes.where("exam_id = 18")
    if !@subscribe
      flash[:notice]  = "Você não se inscreveu para o SIMULADÃO!."
      redirect_to simulateds_path
    else
      @subscribe.update_all(:confirmation => true)
    end
  end

end