class DisplaysController < ApplicationController
  before_filter :load_class
  def index
    if params[:channel_student_id].blank?
      redirect_to root_path
    else
      redirect_to channel_student_path(params[:channel_student_id])
    end
  end

  def show
    @category = Category.find_by_id(5, :conditions => ['published =  true and active = true'])      
    if @category.blank?
      redirect_to root_path
    end
    @type_warnings = TypeWarning.find(:all, :conditions => ['published =  true and active = true']) 
    @warning = Warning.find_by_id(params[:id], :conditions => ['published =  true and active = true and type_warning_id =?', params[:channel_student_id]])
    if @warning.blank?
     redirect_to channel_students_path
    end
  end
end
