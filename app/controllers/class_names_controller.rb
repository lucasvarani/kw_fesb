class ClassNamesController < ApplicationController
  before_filter :load_class 
  def index
  end

  def show
    @class_category = ClassName.find_by_id(params[:id], :conditions => ['active = true'])
    @class_name = ClassName.find_by_id(params[:id], :conditions => ['published =  true and active = true'], :include => :class_images)
    @childrens = @class_name.children
    if @class_name.blank?
      redirect_to class_category_path(@class_category.class_category.url_slug())
    end 
  end
end
