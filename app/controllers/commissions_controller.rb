class CommissionsController < ApplicationController 
  before_filter :load_class 
  def index 
    @category = Category.find_by_id(3, :conditions => ['published =  true and active = true'], :include => :items)  
    if @category.blank?
      redirect_to root_path
    end    
  end

  def show
  end
end
