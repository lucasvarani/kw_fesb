class HomesController < ApplicationController
	before_filter :load_class
	def index
		@warnings_channel = Warning.find(:all, :conditions => ['published =  true and active = true and type_warning_id != 7'], :order => "id desc")
		@warnings = Warning.find(:all, :conditions => ["active = true and published = true and show_home = true"], :order => "id desc", :limit => 12)
		@highlights_top =  Highlight.find(:all, :conditions => ["active = true and published = true and flag = 1"], :order => "id desc")
		@highlights_mid =  Highlight.find(:all, :conditions => ["active = true and published = true and flag = 2"], :order => "id desc")
		@highlights_lat =  Highlight.find(:all, :conditions => ["active = true and published = true and flag = 3"], :order => "id desc")
		@schedules = Schedule.find(:all, :conditions => ["active = true and published = true and schedule_date >= ?", Date.current], :order => "schedule_date ASC", :limit=>5)
		@category = Category.find_by_id(4, :conditions => ['published =  true and active = true'], :include => :items)
		@coral = @category.items.find_by_id(10)
		@nafe = @category.items.find_by_id(6)
		@nutri = @category.items.find_by_id(7)
		@hvet = @category.items.find_by_id(8)
		@ceef = @category.items.find_by_id(524)
		@ggd = @category.items.find_by_id(645)
		@recipe = Recipe.last
	end

	def virtual_tour

	end

	def sae

	end

	def search
		@category = Category.find_by_id(9, :conditions => ['published =  true and active = true'])
		if !params[:search].blank?
			@warnings = Warning.order("created_at desc").all(:conditions => ["active = 1 AND published = 1 AND name like :search or description like :search", {:search => "%#{params[:search]}%"} ])
		else
			@warnings = nil
		end
	end
end
