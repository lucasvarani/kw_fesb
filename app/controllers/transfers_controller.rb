class TransfersController < ApplicationController
  # invisible_captcha :only => :create, :on_spam => :spam_detected
  layout "transfers"
  def index
    @class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 or class_category_id = 2 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
    @class_categories = ClassName.all(:conditions => ["id != 24 and id != 25 and id != 27 and id !=28 and id != 29 and id != 30 and published = 1 and active = 1 and class_category_id = 1 or class_category_id = 2 and show_subscribe = 1 and id not in (?) and pre_subscribe = 0", @class_children.map{|s| s.father_id}], :order => :name)
    @transfer = Transfer.new
  end

  def create
     @class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 or class_category_id = 2 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
    @class_categories = ClassName.all(:conditions => ["id != 24 and id != 25 and id != 27 and id !=28 and id != 29 and id != 30 and published = 1 and active = 1 and class_category_id = 1 or class_category_id = 2 and show_subscribe = 1 and id not in (?) and pre_subscribe = 0", @class_children.map{|s| s.father_id}], :order => :name)
    @transfer = Transfer.new(params[:transfer])
    # if simple_captcha_valid?
    # puts "foi --------------------------------------------------------------------"
    if @transfer.save
      Notifier.send_transfer(@transfer).deliver 
      Notifier.send_transfer_user(@transfer).deliver     
      download 
      # redirect_to transfers_path, :notice => "Successfully created transfer."
    else
      render :action => 'index'
    end
  end

  def download
    data = open("http://fesb.br/transferencia_2sem_2018.pdf") 
    send_data data.read, :filename=> "transferencia_2sem_2018.pdf", :type=> "application/pdf", :disposition=> 'inline', :stream=> 'true', :buffer_size=> '4096' 
  end

  # def show
  #   @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])  
  #   if @category.blank?
  #     redirect_to root_path
  #   end
  # end

  # def spam_detected
  #   redirect_to "https://www.pinnacledisplays.com/images/spam-spambot-by-Jack.jpg", :alert => 'Spam detected'
  # end

end
