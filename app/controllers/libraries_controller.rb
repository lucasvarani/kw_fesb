class LibrariesController < ApplicationController
  before_filter :load_class, :load_cdd
  def index
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @virtual_library = Category.find_by_id(10, :include => :items)
    # @news_library = Category.find_by_id(12, :include => :items)
    # @tcc_nt = Category.find_by_id(13, :include => :items)
    # @sites_interessantes = Category.find_by_id(14, :include => :items)
    @terms = @category.items.find_by_id(407)
    @virtual_libraries = Item.paginate(:page => params[:page], :per_page => 10).all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")
    @tcc_nt = Category.find_by_id(13)
    @tcc_nts = Item.paginate(:page => params[:page], :per_page => 10).all(:conditions => ['published = true and active = true and category_id = ?', @tcc_nt.id], :order => "id desc")
    @sheet = Sheet.new
    @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = 12'], :order => "id desc")
    @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")
  end

  # def show
  #   @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
  #   if @category.blank?
  #     redirect_to root_path
  #   end
  #   @item = @category.items.find_by_id(params[:id])
  #   @virtual_library = Category.find_by_id(10, :include => :items)
  #   @news_library = Category.find_by_id(12, :include => :items)
  #   @tcc_nt = Category.find_by_id(13, :include => :items)
  #   @sites_interessantes = Category.find_by_id(14, :include => :items)
  # end

#   def virtual_library
#     @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
#     if @category.blank?
#       redirect_to root_path
#     end
#     @item = @category.items.find_by_id(params[:id])
#     @virtual_library = Category.find_by_id(10)
#     @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")
# #    @virtual_libraries = Item.paginate(:page => params[:page], :per_page => 30).all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")
#     @news_library = Category.find_by_id(12)
#     @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")

#     @tcc_nt = Category.find_by_id(13)
#     @tcc_nts = Item.all(:conditions => ['published = true and active = true and category_id = ?', @tcc_nt.id], :order => "id desc")

#     @sites_interessantes = Category.find_by_id(14)
#     @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")


#     if !(@virtual_libraries.count > 0)
#       redirect_to libraries_path
#     end
#   end

  def news_libraries
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @news_library = Category.find_by_id(12)
    @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")
#    @library_news = Item.paginate(:page => params[:page], :per_page => 30).all(:conditions => ['published =  true and active = true and category_id = ?',  @library_news.id], :order => "id desc")
    @virtual_library = Category.find_by_id(10)
    @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")

    @sites_interessantes = Category.find_by_id(14)
    @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")


    if !(@news_libraries.count > 0)
      redirect_to libraries_path
    end
  end

  def news_library
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end



    @news_library = Category.find_by_id(10)
    @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")
#    @library_news = Item.paginate(:page => params[:page], :per_page => 30).all(:conditions => ['published =  true and active = true and category_id = ?',  @library_news.id], :order => "id desc")
    @virtual_library = Category.find_by_id(12)
    @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")
    @news_library_item = Item.find_by_id(params[:id])

    @sites_interessantes = Category.find_by_id(14)
    @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")


    if @news_library_item.blank?
      redirect_to news_libraries_libraries_path
    end
  end



#   def tcc_nts
#     @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
#     if @category.blank?
#       redirect_to root_path
#     end

#     @tcc_nt = Category.find_by_id(13)
#     @tcc_nts = Item.all(:conditions => ['published = true and active = true and category_id = ?', @tcc_nt.id], :order => "id desc")

#     @sites_interessantes = Category.find_by_id(14)
#     @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")


#     @news_library = Category.find_by_id(12)
#     @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")
# #    @library_news = Item.paginate(:page => params[:page], :per_page => 30).all(:conditions => ['published =  true and active = true and category_id = ?',  @library_news.id], :order => "id desc")
#     @virtual_library = Category.find_by_id(10)
#     @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")

#     if !(@tcc_nts.count > 0)
#       redirect_to libraries_path
#     end
#   end



#   def tcc_nt
#     @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
#     if @category.blank?
#       redirect_to root_path
#     end

#     @tcc_nt = Category.find_by_id(13)
#     @tcc_nts = Item.all(:conditions => ['published = true and active = true and category_id = ?', @tcc_nt.id], :order => "id desc")

#     @news_library = Category.find_by_id(12)
#     @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")
# #    @library_news = Item.paginate(:page => params[:page], :per_page => 30).all(:conditions => ['published =  true and active = true and category_id = ?',  @library_news.id], :order => "id desc")
#     @virtual_library = Category.find_by_id(10)
#     @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")
#     @tcc_nt_item = @tcc_nt.items.find_by_id(params[:id])

#     @sites_interessantes = Category.find_by_id(14)
#     @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")


#     if @tcc_nt_item.blank?
#       redirect_to news_libraries_libraries_path
#     end
#   end

  def sites_interessantes
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @tcc_nt = Category.find_by_id(13)
    @tcc_nts = Item.all(:conditions => ['published = true and active = true and category_id = ?', @tcc_nt.id], :order => "id desc")

    @sites_interessantes = Category.find_by_id(14)
    @sites_interessantes_itens = Item.all(:conditions => ['published = true and active = true and category_id = ?', @sites_interessantes.id], :order => "id desc")

    @news_library = Category.find_by_id(12)
    @news_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @news_library.id], :order => "id desc")
    @virtual_library = Category.find_by_id(10)
    @virtual_libraries = Item.all(:conditions => ['published =  true and active = true and category_id = ?',  @virtual_library.id], :order => "id desc")


    if @sites_interessantes_itens.blank?
      redirect_to news_libraries_libraries_path
    end
  end

  def load_cdd
    @category = Category.find_by_id(9, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @virtual_library = Category.find_by_id(10, :include => :items)
    @news_library = Category.find_by_id(12, :include => :items)
    @tcc_nt = Category.find_by_id(13, :include => :items)
    @sites_interessantes = Category.find_by_id(14, :include => :items)
    @class_names = ClassName.all(:conditions => ["published =  true and active = true and not cdd is null"]).collect {|c| [c.name, c.id]}
  end
end
