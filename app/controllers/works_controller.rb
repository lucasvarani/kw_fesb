class WorksController < ApplicationController
  before_filter :load_class
  invisible_captcha :only => :create, :on_spam => :spam_detected

  def new
    @category = Category.find_by_id(7, :conditions => ['published =  true and active = true'])  
    if @category.blank?
      redirect_to root_path
    else
      @work = Work.new 
    end
  end
  
  def create
    @category = Category.find_by_id(7, :conditions => ['published =  true and active = true']) 
    if @category.blank?
      redirect_to root_path
    else
      @work = Work.new(params[:work])
      if @work.save
        Notifier.send_work(@work).deliver 
        Notifier.send_work_user(@work).deliver
        redirect_to [@work], :notice => "Successfully created contact."
      else
        render :action => 'new'
      end
    end
  end

  def show
    @category = Category.find_by_id(7, :conditions => ['published =  true and active = true'])  
    if @category.blank?
      redirect_to root_path
    end
  end

  def spam_detected
    redirect_to root_path, :alert => 'Spam detected'
  end

end
