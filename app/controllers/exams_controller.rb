class ExamsController < ApplicationController
  before_filter :load_class, :load_dropdown_list, :redirect_root
  #access_control do
  #  allow :user, :to => [:ticket]
  #  allow :all, :to => [:ticket]
  #  allow anonymous, :to => [:index, :update_dates, :load_dropdown_list, :login]
  #end

  layout 'exams'

  def index
    
  end

  def inscricao
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exams = Exam.all(:conditions => ["published = 1 AND active = 1 AND start_date < ? AND end_date > ? AND id not in(#{@exam_participants.map(&:exam_id).join(',')})", DateTime.now, DateTime.now])
      end
    end
  end

  def simulador
    @class_categories = ClassName.find(:all, :conditions=>["published = true and active = true and class_category_id = 1 and financing_6 is not null and father_id is null"], :order => :name).collect {|c| [c.name, c.id]}    
  end

  def agendado 
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exam_id = Exam.where("exam_type_id = 2 and published = true and active = true and year = #{CURRENT_YEAR_EXAM} AND id not in(#{@exam_participants.map(&:exam_id).join(',')})").last
        if @exam_id
          @exam_id = Exam.where("exam_type_id = 2 and published = true and year = #{CURRENT_YEAR_EXAM}").last
        else
          redirect_to "/vestibular/inscricao"
        end
      else
        @exam_id = Exam.where("exam_type_id = 2 and published = true and year = #{CURRENT_YEAR_EXAM}").last
      end
    else
      @exam_id = Exam.where("exam_type_id = 2 and published = true and year = #{CURRENT_YEAR_EXAM}").last
    end
  end

  def update_prices
    @class = ClassName.find(params[:id])
    @childrens = @class.children
    @last = @childrens.last
    @first = @childrens.first
  end

  def update_dates
    @exam = Exam.find(params[:id])
    @exam_dates = ExamDate.all(:conditions => ["exam_id = ? AND published = 1 AND active = 1", @exam.id]).map{|s| [["Data: ", I18n.localize(s.exam_date, :format=>:short)," - " , "Cidade: ", s.itinerant_exam.city], s.id]}
  end
  #{I18n.localize(s.exam_date, :format=>:short)}

  def load_dropdown_list
    @exam_dates = ExamDate.all_published
  end

  def redirect_root
    redirect_to :root
  end

  def login
    if !current_user
      @user_session = UserSession.new
    else
      @user = User.find(current_user.id)
      check_subscribes
    end
  end

  def ticket
    redirect_to exams_path
    # if !current_user
    #  redirect_to login_exams_path
    # else
    #   @user = User.find(current_user.id)
    #   check_subscribes
    # end
  end

  def find_result
    @user = User.find_by_cpf(params[:cpf])
    if !@user.blank?
      check_subscribes
    else
      flash[:notice]  = "CPF inválido tente novamente."
      redirect_to exams_path
    end
  end

  def check_subscribes
    @subscribes = @user.subscribes.all(:conditions=> ["year = ?", CURRENT_YEAR_EXAM])
    if @subscribes.count > 0
      date_now = Date.today
      @subscribes.each do |subscribe|

        if !subscribe.paid
          if subscribe.ticket_link.present?
            date_link = subscribe.ticket_link.scan(/dtvc\=([^\&]+)/).to_s
            date_link = DateTime.strptime(date_link, '%d%m%Y')
            if ((date_link < date_now) && (UPDATE_ALT == true))
              update_ticket(subscribe.id, 2)
              $update_subscribes_show = true
            end
          end
        end

      end
      if $update_subscribes_show
        @subscribes = @user.subscribes.all(:conditions=> ["year = ?", CURRENT_YEAR_EXAM])
      end
    end
  end

  def my_email

  end

  def find_my_email
    @user = User.find_by_cpf(params[:cpf])
  end

  def redirect_simuladao    
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exam_id = Exam.where("exam_type_id = 1 and published = true and year = #{CURRENT_YEAR_EXAM} AND id not in(#{@exam_participants.map(&:exam_id).join(',')})").last
        if @exam_id
          redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
        else
          redirect_to "/vestibular/inscricao"
        end
      end
    else
      @exam_id = Exam.where("exam_type_id = 4 and published = true and year = #{CURRENT_YEAR_EXAM}").last
      redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
    end
  end

  def redirect_tradicional    
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exam_id = Exam.where("exam_type_id = 1 and published = true and year = #{CURRENT_YEAR_EXAM} AND id not in(#{@exam_participants.map(&:exam_id).join(',')})").last
        if @exam_id
          redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
        else
          redirect_to "/vestibular/inscricao"
        end
      end
    else
      @exam_id = Exam.where("exam_type_id = 1 and published = true and year = #{CURRENT_YEAR_EXAM}").last
      redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
    end
  end

  def redirect_enem    
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exam_id = Exam.where("exam_type_id = 1 and published = true and year = #{CURRENT_YEAR_EXAM} AND id not in(#{@exam_participants.map(&:exam_id).join(',')})").last
        if @exam_id
          redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
        else
          @exam_id = Exam.where("exam_type_id = 3 and published = true and year = #{CURRENT_YEAR_EXAM}").last
          redirect_to "/vestibular/inscricao"
        end
      end
    else
      @exam_id = Exam.where("exam_type_id = 3 and published = true and year = #{CURRENT_YEAR_EXAM}").last
      redirect_to "/socioeconomics/new?[exam_id]=#{@exam_id.id}"
    end
  end
end
