class Admin::SchedulesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  

  layout "admin"
  before_filter :blank_time, :only=> [:create, :update]
  
  def index
    @schedules = Schedule.order('id DESC').all
  end

  def show
    @schedule = Schedule.find(params[:id])
  end

  def new
    @schedule = Schedule.new
  end

  def create
    @schedule = Schedule.new(params[:schedule])
    if @schedule.save
      redirect_to [:admin, @schedule], :notice => "Successfully created schedule."
    else
      render :action => 'new'
    end
  end

  def edit
    @schedule = Schedule.find(params[:id])
  end

  def update
    @schedule = Schedule.find(params[:id])
    if @schedule.update_attributes(params[:schedule])
      redirect_to [:admin, @schedule], :notice  => "Successfully updated schedule."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @schedule = Schedule.find(params[:id])
    @schedule.destroy
    redirect_to admin_schedules_url, :notice => "Successfully destroyed schedule."
  end
  
  def blank_time
    if params[:schedule]['schedule_time(4i)'].blank?
        params[:schedule]['schedule_time(1i)'] = ""
        params[:schedule]['schedule_time(2i)'] = ""
        params[:schedule]['schedule_time(3i)'] = ""
        params[:schedule]['schedule_time(4i)'] = ""
        params[:schedule]['schedule_time(5i)'] = ""
    end
  end
end
