class Admin::ItemImagesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  before_filter :load_item

  def index
    @item_images = @item.item_images.all_active
  end

  def show
    @item_image = @item.item_images.find(params[:id])
  end

  def new
    @item_image = @item.item_images.build
  end

  def create
    @item_image = @item.item_images.build(params[:item_image])
    if @item_image.save
      redirect_to [:admin, @category, @item, @item_image], :notice => "Successfully created item image."
    else
      render :action => 'new'
    end
  end

  def edit
    @item_image = @item.item_images.find(params[:id])
  end

  def update
    @item_image = @item.item_images.find(params[:id])
    if @item_image.update_attributes(params[:item_image])
      redirect_to [:admin, @category, @item, @item_image], :notice  => "Successfully updated item image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @item_image = @item.item_images.find(params[:id])
    @item_image.newdestroy
    redirect_to admin_category_item_item_images_path(@category, @item), :notice => "Successfully destroyed item image."
  end
  
  def load_item
    @category = Category.find_by_id(params[:category_id])  
    @item = @category.items.find_by_id(params[:item_id])
  end
  
  
end
