class Admin::ClassCategoriesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"

  def index
    @class_categories = ClassCategory.all_active
  end

  def show
    @class_category = ClassCategory.find(params[:id])
  end

  def new
    @class_category = ClassCategory.new
  end

  def create
    @class_category = ClassCategory.new(params[:class_category])
    if @class_category.save
      redirect_to [:admin, @class_category], :notice => "Successfully created class category."
    else
      render :action => 'new'
    end
  end

  def edit
    @class_category = ClassCategory.find(params[:id])
  end

  def update
    @class_category = ClassCategory.find(params[:id])
    if @class_category.update_attributes(params[:class_category])
      redirect_to [:admin, @class_category], :notice  => "Successfully updated class category."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @class_category = ClassCategory.find(params[:id])
    @class_category.newdestroy
    redirect_to admin_class_categories_url, :notice => "Successfully destroyed class category."
  end
end
