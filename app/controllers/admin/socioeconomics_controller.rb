class Admin::SocioeconomicsController < ApplicationController
  def index
    @socioeconomics = Socioeconomic.all
  end

  def show
    @socioeconomic = Socioeconomic.find(params[:id])
  end

  def new
    @socioeconomic = Socioeconomic.new
  end

  def create
    @socioeconomic = Socioeconomic.new(params[:socioeconomic])
    if @socioeconomic.save
      redirect_to [:admin, @socioeconomic], :notice => "Successfully created socioeconomic."
    else
      render :action => 'new'
    end
  end

  def edit
    @socioeconomic = Socioeconomic.find(params[:id])
  end

  def update
    @socioeconomic = Socioeconomic.find(params[:id])
    if @socioeconomic.update_attributes(params[:socioeconomic])
      redirect_to [:admin, @socioeconomic], :notice  => "Successfully updated socioeconomic."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @socioeconomic = Socioeconomic.find(params[:id])
    @socioeconomic.destroy
    redirect_to admin_socioeconomics_url, :notice => "Successfully destroyed socioeconomic."
  end
end
