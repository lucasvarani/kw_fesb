class Admin::ClassNamesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :library, :to => [:index, :edit, :update,:show]
  end
  
  layout "admin"
  before_filter :load_category, :only => [:new, :edit]
  
  def index
    @class_names = ClassName.where("active =  true and father_id is null").order('id desc')
    @childrens = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and father_id is not null"], :group => "father_id")
  end

  def show
    @class_name = ClassName.find(params[:id])
    @childrens = @class_name.children
  end

  def new
    @class_name = ClassName.new
  end

  def create
    @class_name = ClassName.new(params[:class_name])
    if @class_name.save
      redirect_to [:admin, @class_name], :notice => "Successfully created class name."
    else
      render :action => 'new'
    end
  end

  def edit
    @class_name = ClassName.find(params[:id])
  end

  def update
    if params[:class_name][:teacher_ids].nil?
      params[:class_name][:teacher_ids] = []
    end
    @class_name = ClassName.find(params[:id])
    if @class_name.update_attributes(params[:class_name])
      redirect_to [:admin, @class_name], :notice  => "Successfully updated class name."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @class_name = ClassName.find(params[:id])
    @class_name.newdestroy
    redirect_to admin_class_names_url, :notice => "Successfully destroyed class name."
  end
  
  def load_category
    @class_categories = ClassCategory.all_published.collect {|c| [c.name, c.id]}  
    @class_styles = ClassStyle.all_published.collect {|c| [c.name, c.id]} 
    @teachers = Teacher.all_published
    @coordinators = @teachers.collect {|c| [c.name, c.id]} 
  end
  
  def adjusts_period
    @class_name = ClassName.find(params[:id])
    if params[:children].to_i == 0
      @period = ClassName.new
    else
      @period = ClassName.find(params[:children])        
    end  
  end
  
  def save_adjusts_period
    @class_name = ClassName.find(params[:id])
    if params[:children].to_i == 0
      @period = ClassName.new(params[:class_name])
      @period.father_id = @class_name.id
      @period.class_category_id = @class_name.class_category_id
      @period.class_style_id = @class_name.class_style_id
      @period.save
    else
      @period = ClassName.find(params[:children])   
      @period.update_attributes(params[:class_name])     
    end
    @childrens = @class_name.children
  end
  
  def disable_all_subscribe
    ClassName.update_all(:show_subscribe => false)
    redirect_to admin_class_names_url, :notice => "Todos os cursos foram desabilitados."
  end
  
  def enable_all_subscribe
    ClassName.update_all(:show_subscribe => true)
    redirect_to admin_class_names_url, :notice => "Todos os cursos foram habilitados."
  end
end
