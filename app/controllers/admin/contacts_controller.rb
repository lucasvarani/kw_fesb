class Admin::ContactsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end

  layout "admin"
  before_filter :load_department
  def index
    @contacts = Contact.all_active("id desc")
  end

  def show
    @contact = Contact.find(params[:id])
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(params[:contact])
    if @contact.save
      redirect_to [:admin, @contact], :notice => "Successfully created contact."
    else
      render :action => 'new'
    end
  end

  def edit
    @contact = Contact.find(params[:id])
  end

  def update
    @contact = Contact.find(params[:id])
    if @contact.update_attributes(params[:contact])
      redirect_to [:admin, @contact], :notice  => "Successfully updated contact."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @contact = Contact.find(params[:id])
    @contact.newdestroy
    redirect_to admin_contacts_url, :notice => "Successfully destroyed contact."
  end

  def load_department
    @department = Department.all_published.collect {|c| [c.name, c.id]}
  end
end
