class Admin::UsersController < ApplicationController
  access_control do
      allow :administrator, :all
  end
  
  layout "admin"
  
  def index
    @users = User.all_cms_users
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
    
  end

  def create
    @user = User.new(params[:user])
    @user.cms_user = false
    #@user.role = "user"
    if @user.save
      redirect_to admin_users_path, :notice => "Usuário criado com sucesso"
    else
      render :action => 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    @user.cms_user = false
    if @user.update_attributes(params[:user])
      redirect_to admin_users_path, :notice  => "Usuário atualizado com sucesso."
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to admin_users_url, :notice => "Usuário deletado com sucesso."
  end

end
