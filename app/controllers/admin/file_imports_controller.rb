class Admin::FileImportsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :management
  end
  
  layout "admin"
  
  def index
    @file_imports = FileImport.all
  end

  def show
    @file_import = FileImport.find(params[:id])
  end

  def new
    @file_import = FileImport.new
  end

  def create
    @file_import = FileImport.new(params[:file_import])
    if @file_import.save
      resultado = ""
      erro = ""
      count = 0
      FasterCSV.foreach(@file_import.fileimport.path, :quote_char => '"', :col_sep =>';', :row_sep =>:auto) do |row|
        puts "Numero da Inscricao: " + row[1].to_s
        @subscribe = Subscribe.find_by_id(row[1].to_i, :include => :user) if count != 0
        if !@subscribe.blank?
          @subscribe.note_issues = row[36].to_i
          @subscribe.weight_issues = (100*@subscribe.note_issues/36)/3       
          @subscribe.save
          resultado = resultado + "<b>ID no arquivo:</b> #{row[0]} &nbsp;&nbsp;<b>Nome:</b> #{@subscribe.user.name} &nbsp;&nbsp;<b>Incriçao no arquivo:</b> #{row[1]} &nbsp;&nbsp;<b>Incriçao no banco:</b> #{@subscribe.id} &nbsp;&nbsp;<b>Nota:</b> #{@subscribe.note_issues} <br />"
        else
          erro = erro + "<b>ID no arquivo:</b> #{row[0]} &nbsp;&nbsp;<b>Incriçao no arquivo:</b> #{row[1]} &nbsp;&nbsp;<b>Nota:</b> #{row[36]} <br />" if count != 0
        end
        count = count + 1
      end
      @file_import.log = resultado
      @file_import.error = erro
      @file_import.user_id = current_user.id
      @file_import.save
      
      redirect_to [:admin, @file_import], :notice => "Successfully created file import."
    else
      render :action => 'new'
    end
  end

  def edit
    @file_import = FileImport.find(params[:id])
  end

  def update
    @file_import = FileImport.find(params[:id])
    if @file_import.update_attributes(params[:file_import])
      redirect_to [:admin, @file_import], :notice  => "Successfully updated file import."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @file_import = FileImport.find(params[:id])
    @file_import.destroy
    redirect_to admin_file_imports_url, :notice => "Successfully destroyed file import."
  end
end
