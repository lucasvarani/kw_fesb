class Admin::SheetsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :library, :all
  end
  before_filter :load_cdd, :except => [:index, :destroy]
  layout "admin"
  def index
    @sheets = Sheet.all(:order => "id desc")
  end

  def show
    @sheet = Sheet.find(params[:id])
    respond_to do |format|
       format.html # show.html.erb
       format.xml { render :xml => @sheet }
       format.msword { set_header('msword', "#{@sheet.name}.doc") }
    end
  end

  def new
    @sheet = Sheet.new
  end

  def create
    @sheet = Sheet.new(params[:sheet])
    if @sheet.save
      redirect_to [:admin, @sheet], :notice => "Successfully created sheet."
    else
      render :action => 'new'
    end
  end

  def edit
    @sheet = Sheet.find(params[:id])
  end

  def update
    @sheet = Sheet.find(params[:id])
    if @sheet.update_attributes(params[:sheet])
      redirect_to [:admin, @sheet], :notice  => "Successfully updated sheet."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @sheet = Sheet.find(params[:id])
    @sheet.destroy
    redirect_to admin_sheets_url, :notice => "Successfully destroyed sheet."
  end
  
  def load_cdd
    @class_names = ClassName.all(:conditions => ["published =  true and active = true and not cdd is null"]).collect {|c| [c.name, c.id]}  
  end
  
  def export_csv
    @sheet = Sheet.find(params[:id])
    name_show = @sheet.name.split(",").first
    i = 1
    keywords_show =""
    @sheet.keywords.split(",").each do |keyword|
      keywords_show += "#{i.to_s}. #{keyword}. "
      i += 1
    end
    
    csv_string = FasterCSV.generate(:col_sep => ";") do |csv| 
      csv << ["codigo_sistema", "name", "email", "ra", "author", "pha_ficha", "codigo_nome_ficha", "nome_ficha", "cabecalho_ficha", "pag_ilustracao_ficha", "orientador_ficha", "curso_ficha",
              "keywords_ficha", "rodape_ficha", "data_ficha", "cdd_ficha"] 
      csv << [@sheet.id, @sheet.name, @sheet.email, @sheet.ra, @sheet.author, @sheet.class_name.pha, @sheet.code_name, view_context.inverte_sobrenome(name_show),
              "#{@sheet.title_subtitle} / #{@sheet.name.split(",").first}.Bragança Paulista, SP: FESB,#{@sheet.created_at.strftime("%Y")}.", 
              "#{@sheet.number_pages.to_s} p. #{@sheet.number_illustrations.to_s} il.",@sheet.advisor, @sheet.class_name.name, keywords_show,
              "I. #{@sheet.advisor}. II. #{@sheet.name}. III. Faculdade de Ciências e Letras de Bragança Paulista, #{@sheet.class_name.name}. IV. Título.",
              @sheet.created_at.strftime("%d.%m.%y"), @sheet.class_name.cdd] 
      # data rows 
      #@sheet.each do |sheet| 
      #  csv << [sheet.id, sheet.name, sheet.email, sheet.ra, sheet.author] 
      #end 
    end 

    # send it to the browsah
    send_data csv_string, 
              :type => 'text/csv; charset=utf-8; header=present', 
              :disposition => "attachment; filename=FichaCatalografica_#{@sheet.id}.csv"
    
        
  end
end
