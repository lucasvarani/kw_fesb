class Admin::ExamsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :management
  end

  layout "admin"

  def index
    @exams = Exam.all_active("year desc, id desc")
  end

  def show
    @exam = Exam.find(params[:id])
  end

  def new
    @exam = Exam.new(:year => CURRENT_YEAR_EXAM)
  end

  def create
    @exam = Exam.new(params[:exam])
    if @exam.save
      redirect_to [:admin, @exam], :notice => "Successfully created exam."
    else
      render :action => 'new'
    end
  end

  def edit
    @exam = Exam.find(params[:id])
  end

  def update
    @exam = Exam.find(params[:id])
    if @exam.update_attributes(params[:exam])
      redirect_to [:admin, @exam], :notice  => "Successfully updated exam."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @exam = Exam.find(params[:id])
    @exam.newdestroy
    redirect_to admin_exams_url, :notice => "Successfully destroyed exam."
  end

  def report
    @exam = Exam.find(params[:id])
    @reports = @exam.subscribes.all(:include => [:user, :exam_date, :first_option_class, :second_option_class])
  end
  def import_file
    @exam = Exam.find(params[:id])
    @print = "Nada"
    @resultado = ""
    @erro = ""
    if File.exists?('public/import/Resultado.csv')
      count = 0
      FasterCSV.foreach("public/import/Resultado.csv", :quote_char => '"', :col_sep =>';', :row_sep =>:auto) do |row|
        @subscribe = Subscribe.find(row[1].to_i, :include => :user) if count != 0
        if !@subscribe.blank?
          @subscribe.note_issues = row[37].to_i
          @subscribe.weight_issues = (100*@subscribe.note_issues/34)/3
          @subscribe.save
          @resultado = @resultado + "ID no arquivo: #{row[0]} \tNome: #{@subscribe.user.name} \tIncriçao no arquivo: #{row[1]} \tIncriçao no banco: #{@subscribe.id} \tNota: #{@subscribe.note_issues} \n\n"
        else
          @erro = @erro + "ID no arquivo: #{row[0]}\t Incriçao no arquivo: #{row[1]}\tNota: #{row[37]}\n" if count != 0
        end
        count = count + 1
      end
    end
  end


  def launch_note
    @subscribe = Subscribe.find(params[:id])
  end
  def processing_note
    @subscribe = Subscribe.find(params[:id])
    if @subscribe.update_attributes(params[:subscribe])
      if !@subscribe.copywriting_note.blank?
        @subscribe.copywriting_weight = (@subscribe.copywriting_note/3)*2
      end
      if !@subscribe.note_issues.blank?
        @subscribe.weight_issues = (100*@subscribe.note_issues/34)/3
      end
      if !@subscribe.note_issues.blank? && !@subscribe.copywriting_note.blank?
        @subscribe.endnote = (@subscribe.copywriting_weight + @subscribe.weight_issues)
      end
      @subscribe.save
      redirect_to show_note_admin_exam_path(@subscribe)
    else
      render :action => 'launch_note'
    end
  end
  def show_note
    @subscribe = Subscribe.find(params[:id])
  end

  def ajust_message
    @subscribe = Subscribe.find(params[:id])
    @subscribe.subscribe_message = params[:text]
    @subscribe.registrationshow = params[:show].to_bool
    @subscribe.save
  end

  def print_selected
    d = DateTime.now.strftime("%d-%m-%Y")
    tipo_de_relatorio = params[:print_type]
    puts tipo_de_relatorio
    case tipo_de_relatorio.to_s
      #Folha de Redação por usuário
      when "user_fol_redacao"
        @subscribes = Subscribe.all(:conditions => ["paid = true and id = ?",params[:user_id]] )
        render  :pdf          => "Folha de Redacao-#{@subscribes.first.registration}",
          :layout       => "grid_sheet.html.haml",
          :template     => "/admin/exams/writing_sheet.html.haml",
          :show_as_html => params[:debug].present?,
          :margin       => {:top                => 54,                     # default 10 (mm)
                            :bottom             => 20,
                            :left               => 30,
                            :right              => 15},
          :page_size => 'A4'
      #Folha de Redação por seleção
      when "fol_redacao"
        @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.id in (?)", params[:subscrib_ids]])
        render  :pdf          => "Folha de Redacao-#{d}",
          :layout       => "grid_sheet.html.haml",
          :template     => "/admin/exams/writing_sheet.html.haml",
          :show_as_html => params[:debug].present?,
          :margin       => {:top                => 54,                     # default 10 (mm)
                            :bottom             => 20,
                            :left               => 30,
                            :right              => 15},
          :page_size => 'A4'
      #Folha de Redação para todos os pagos do vestibular
      when "fol_redacao_all"
        params_date = params[:subscribe_date].to_s
        if  params_date == 0.to_s
          @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.exam_id = ?", params[:subscribe]],:include => [:user, :exam_date, :first_option_class])
        else
          @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.exam_id = ? and subscribes.exam_date_id = ?", params[:subscribe], params[:subscribe_date]],:include => [:user, :exam_date, :first_option_class])
        end
        render  :pdf          => "Folha de Redacao-#{d}",
          :layout       => "grid_sheet.html.haml",
          :template     => "/admin/exams/writing_sheet.html.haml",
          :show_as_html => params[:debug].present?,
          :margin       => {:top                => 54,                     # default 10 (mm)
                            :bottom             => 20,
                            :left               => 30,
                            :right              => 15},
          :page_size => 'A4'



      #Gabarito por usuário
      when "user_gabarito"
        @subscribes = Subscribe.all(:conditions => ["paid = true and id = ?",params[:user_id]] )
        render  :pdf          => "Gabarito-#{@subscribes.first.registration}",
                :layout       => "grid_sheet.html.haml",
                :template     => "/admin/exams/generate_pdf.html.haml",
                :show_as_html => params[:debug].present?,
                :margin       => {:top                => 30,                     # default 10 (mm)
                                  :bottom             => 20,
                                  :left               => 70,
                                  :right              => 15},
                :page_size => 'A4'
      #Gabarito por seleção
      when "gabarito"
        @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.id in (?)", params[:subscrib_ids]])
        render  :pdf          => "Gabarito-#{d}",
                :layout       => "grid_sheet.html.haml",
                :template     => "/admin/exams/generate_pdf.html.haml",
                :show_as_html => params[:debug].present?,
                :margin       => {:top                => 30,                     # default 10 (mm)
                                  :bottom             => 20,
                                  :left               => 70,
                                  :right              => 15},
                :page_size => 'A4'
      #Gabarito para todos os pagos do vestibular
      when "gabarito_all"
        params_date = params[:subscribe_date].to_s
        if  params_date == 0.to_s
          @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.exam_id = ?", params[:subscribe]],:include => [:user, :exam_date, :first_option_class])
        else
          @subscribes = Subscribe.joins(:user).order("users.name").all(:conditions => ["subscribes.paid = true and subscribes.exam_id = ? and subscribes.exam_date_id = ?", params[:subscribe], params[:subscribe_date]],:include => [:user, :exam_date, :first_option_class])

        end
        render  :pdf          => "Gabarito-#{d}",
                :layout       => "grid_sheet.html.haml",
                :template     => "/admin/exams/generate_pdf.html.haml",
                :show_as_html => params[:debug].present?,
                :margin       => {:top                => 30,                     # default 10 (mm)
                                  :bottom             => 20,
                                  :left               => 70,
                                  :right              => 15},
                :page_size => 'A4'





      #Escape
      else
        redirect_to admin_exams_url
      end

  end

end
