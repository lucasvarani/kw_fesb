class Admin::TypeWarningsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  
    
  def index
    @type_warnings = TypeWarning.all_active
  end

  def show
    @type_warning = TypeWarning.find(params[:id])
  end

  def new
    @type_warning = TypeWarning.new
  end

  def create
    @type_warning = TypeWarning.new(params[:type_warning])
    if @type_warning.save
      redirect_to [:admin, @type_warning], :notice => "Successfully created type warning."
    else
      render :action => 'new'
    end
  end

  def edit
    @type_warning = TypeWarning.find(params[:id])
  end

  def update
    @type_warning = TypeWarning.find(params[:id])
    if @type_warning.update_attributes(params[:type_warning])
      redirect_to [:admin, @type_warning], :notice  => "Successfully updated type warning."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @type_warning = TypeWarning.find(params[:id])
    @type_warning.newdestroy
    redirect_to admin_type_warnings_url, :notice => "Successfully destroyed type warning."
  end
end
