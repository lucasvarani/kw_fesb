class Admin::ExamDatesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :management
  end

  layout "admin"
  before_filter :load_exam

  def index
    @exam_dates = @exam.exam_dates.all_active
  end

  def show
    @exam_date = @exam.exam_dates.find(params[:id])
  end

  def new
    @exam_date = @exam.exam_dates.new
  end

  def create
    @exam_date = @exam.exam_dates.build(params[:exam_date])
    if @exam_date.save
      redirect_to [:admin,@exam,  @exam_date], :notice => "Successfully created exam date."
    else
      render :action => 'new'
    end
  end

  def edit
    @exam_date = @exam.exam_dates.find(params[:id])
  end

  def update
    @exam_date = @exam.exam_dates.find(params[:id])
    if @exam_date.update_attributes(params[:exam_date])
      redirect_to [:admin, @exam,  @exam_date], :notice  => "Successfully updated exam date."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @exam_date = @exam.exam_dates.find(params[:id])
    @exam_date.newdestroy
    redirect_to admin_exam_exam_dates_url(@exam), :notice => "Successfully destroyed exam date."
  end

  def load_exam
    @exam = Exam.find(params[:exam_id])
  end

  def report
    @exam = Exam.find(params[:exam_id])
    @exam_date = @exam.exam_dates.find(params[:id])
    @reports = @exam.subscribes.all(:conditions => ["exam_date_id = ?", params[:id]], :include => [:user, :exam_date, :first_option_class, :second_option_class])
  end

  def presence
    @exam = Exam.find(params[:exam_id])
    @reports = @exam.subscribes.all(:conditions => ["exam_date_id = ? and paid = true", params[:id]], :include => [:user, :exam_date, :first_option_class, :second_option_class])
  end
  def not_paid
    @exam = Exam.find(params[:exam_id])
    @reports = @exam.subscribes.all(:conditions => ["exam_date_id = ? and paid = false", params[:id]], :include => [:user, :exam_date, :first_option_class, :second_option_class])
  end
  def launch_note
    @exam = Exam.find(params[:exam_id])
    @subscribe = Subscribe.find(params[:id])
  end
  def processing_note
    @exam = Exam.find(params[:exam_id])
    @subscribe = Subscribe.find(params[:id])
    if @subscribe.update_attributes(params[:subscribe])
      @subscribe.copywriting_weight = (@subscribe.copywriting_note/3)*2
      @subscribe.weight_issues = (100*@subscribe.note_issues/34)/3
      @subscribe.endnote = (@subscribe.copywriting_weight + @subscribe.weight_issues)
      @subscribe.save
      redirect_to show_note_admin_exam_exam_date_url(@exam,@subscribe)
    else
      render :action => 'launch_note'
    end
  end
  def show_note
    @exam = Exam.find(params[:exam_id])
    @subscribe = Subscribe.find(params[:id])
  end

  def results
    @exam = Exam.find(params[:exam_id])
    @reports = @exam.subscribes.all(:conditions => ["endnote > 0"], :include => [:user, :exam_date, :first_option_class, :second_option_class])
  end
end
