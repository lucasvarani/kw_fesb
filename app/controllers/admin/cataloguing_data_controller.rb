class Admin::CataloguingDataController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  def index
    @cataloguing_data = CataloguingData.all
  end

  def show
    @cataloguing_data = CataloguingData.find(params[:id])
  end

  def new
    @cataloguing_data = CataloguingData.new
  end

  def create
    @cataloguing_data = CataloguingData.new(params[:cataloguing_data])
    if @cataloguing_data.save
      redirect_to [:admin, @cataloguing_data], :notice => "Successfully created cataloguing data."
    else
      render :action => 'new'
    end
  end

  def edit
    @cataloguing_data = CataloguingData.find(params[:id])
  end

  def update
    @cataloguing_data = CataloguingData.find(params[:id])
    if @cataloguing_data.update_attributes(params[:cataloguing_data])
      redirect_to [:admin, @cataloguing_data], :notice  => "Successfully updated cataloguing data."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @cataloguing_data = CataloguingData.find(params[:id])
    @cataloguing_data.destroy
    redirect_to admin_cataloguing_data_url, :notice => "Successfully destroyed cataloguing data."
  end
end
