class Admin::ClassStylesController < ApplicationController 
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  def index
    @class_styles = ClassStyle.all
  end

  def show
    @class_style = ClassStyle.find(params[:id])
  end

  def new
    @class_style = ClassStyle.new
  end

  def create
    @class_style = ClassStyle.new(params[:class_style])
    if @class_style.save
      redirect_to [:admin, @class_style], :notice => "Successfully created class style."
    else
      render :action => 'new'
    end
  end

  def edit
    @class_style = ClassStyle.find(params[:id])
  end

  def update
    @class_style = ClassStyle.find(params[:id])
    if @class_style.update_attributes(params[:class_style])
      redirect_to [:admin, @class_style], :notice  => "Successfully updated class style."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @class_style = ClassStyle.find(params[:id])
    @class_style.destroy
    redirect_to admin_class_styles_url, :notice => "Successfully destroyed class style."
  end
end
