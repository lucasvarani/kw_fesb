class Admin::AwardGalleriesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  before_filter :load_award
   
  
  def index
    @award_galleries = @award.award_galleries.all_active
  end

  def show
    @award_gallery = @award.award_galleries.find(params[:id])
  end

  def new
    @award_gallery = @award.award_galleries.build
  end

  def create
    @award_gallery = @award.award_galleries.build(params[:award_gallery])
    if @award_gallery.save
      redirect_to [:admin, @award, @award_gallery], :notice => "Successfully created award gallery."
    else
      render :action => 'new'
    end
  end

  def edit
    @award_gallery = @award.award_galleries.find(params[:id])
  end

  def update
    @award_gallery = @award.award_galleries.find(params[:id])
    if @award_gallery.update_attributes(params[:award_gallery])
      redirect_to [:admin, @award, @award_gallery], :notice  => "Successfully updated award gallery."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @award_gallery = @award.award_galleries.find(params[:id])
    @award_gallery.newdestroy
    redirect_to admin_award_award_galleries_url, :notice => "Successfully destroyed award gallery."
  end
  
  def load_award
    @award= Award.find_by_id(params[:award_id])
  end
end
