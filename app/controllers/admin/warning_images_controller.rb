class Admin::WarningImagesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  before_filter :load_warning
  
  def index
    @warning_images = @warning.warning_images.all_active
  end

  def show
    @warning_image = @warning.warning_images.find(params[:id])
  end

  def new
    @warning_image = @warning.warning_images.build
  end

  def create
    @warning_image = @warning.warning_images.build(params[:warning_image])
    if @warning_image.save
      redirect_to [:admin, @warning, @warning_image], :notice => "Successfully created warning image."
    else
      render :action => 'new'
    end
  end

  def edit
    @warning_image = @warning.warning_images.find(params[:id])
  end

  def update
    @warning_image = @warning.warning_images.find(params[:id])
    if @warning_image.update_attributes(params[:warning_image])
      redirect_to [:admin, @warning, @warning_image], :notice  => "Successfully updated warning image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @warning_image = @warning.warning_images.find(params[:id])
    @warning_image.newdestroy
    redirect_to admin_warning_warning_images_url, :notice => "Successfully destroyed warning image."
  end
  
  def load_warning
    @warning = Warning.find_by_id(params[:warning_id])
  end
end
