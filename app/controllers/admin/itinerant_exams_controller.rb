class Admin::ItinerantExamsController < ApplicationController
  layout "admin"
  def index
    @itinerant_exams = ItinerantExam.all
  end

  def show
    @itinerant_exam = ItinerantExam.find(params[:id])
  end

  def new
    @itinerant_exam = ItinerantExam.new
  end

  def create
    @itinerant_exam = ItinerantExam.new(params[:itinerant_exam])
    if @itinerant_exam.save
      redirect_to [:admin, @itinerant_exam], :notice => "Successfully created itinerant_exam."
    else
      render :action => 'new'
    end
  end

  def edit
    @itinerant_exam = ItinerantExam.find(params[:id])
  end

  def update
    @itinerant_exam = ItinerantExam.find(params[:id])
    if @itinerant_exam.update_attributes(params[:itinerant_exam])
      redirect_to [:admin, @itinerant_exam], :notice  => "Successfully updated itinerant_exam."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @itinerant_exam = ItinerantExam.find(params[:id])
    @itinerant_exam.destroy
    redirect_to admin_cities_url, :notice => "Successfully destroyed itinerant_exam."
  end
end
