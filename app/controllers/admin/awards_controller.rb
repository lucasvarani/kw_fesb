class Admin::AwardsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  
  def index
    @awards = Award.all_active
  end

  def show
    @award = Award.find(params[:id])
  end

  def new
    @award = Award.new
  end

  def create
    @award = Award.new(params[:award])
    if @award.save
      redirect_to [:admin, @award], :notice => "Successfully created award."
    else
      render :action => 'new'
    end
  end

  def edit
    @award = Award.find(params[:id])
  end

  def update
    @award = Award.find(params[:id])
    if @award.update_attributes(params[:award])
      redirect_to [:admin, @award], :notice  => "Successfully updated award."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @award = Award.find(params[:id])
    @award.newdestroy
    redirect_to admin_awards_url, :notice => "Successfully destroyed award."
  end
end
