class Admin::StaticsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
      allow :library, :all
      allow :management, :all
  end

  layout "admin"

  def index
    @exam_types = Exam.where("exam_type_id <> 4")
    @subscribes = Subscribe.find(:all, :select => "count(*) as amount, DATE(`subscribes`.`created_at`) as date_created", :group => "date_created", :conditions=> ["year = ? and exam_id in(?)", CURRENT_YEAR_EXAM, @exam_types])
    @subscribesLastYearPaid = ActiveRecord::Base.connection.exec_query("SELECT * FROM subscribes_#{LAST_YEAR_EXAM} s, exams e where e.exam_type_id != 4 and s.paid = true and e.id in(s.exam_id)")
    @subscribesLastYearEnem = ActiveRecord::Base.connection.exec_query("SELECT * FROM subscribes_#{LAST_YEAR_EXAM} s, exams e where e.exam_type_id = 3 and e.id in(s.exam_id)")
    @subscribesLastYear = ActiveRecord::Base.connection.exec_query("SELECT * FROM subscribes_#{LAST_YEAR_EXAM} s, exams e where e.exam_type_id != 4 and e.id in(s.exam_id)")
    #@subscribes_paid = Subscribe.find(:all, :select => "count(*) as amount, DATE(`subscribes`.`updated_at`) as date_updated", :conditions=>["`subscribes`.`paid`= true "],:group => "date_updated")
    #@stuff = Subscribe.all.group_by { |c| c.created_at.to_date }
    @subscribes_all = Subscribe.all(:include => :exam, :conditions=> ["subscribes.year = ? and exams.exam_type_id != 4", CURRENT_YEAR_EXAM])
    @subscribes_paid = Subscribe.all(:include => :exam, :conditions=>["`subscribes`.`paid`= true  and subscribes.year = ? and exams.exam_type_id != 4", CURRENT_YEAR_EXAM])
    @subscribes_attended = Subscribe.all(:include => :exam, :conditions=>["`subscribes`.`endnote` <> '' and subscribes.year = ? and exams.exam_type_id != 4", CURRENT_YEAR_EXAM])
    @subscribes_aproved =  Subscribe.all(:include => :exam, :conditions=>["`subscribes`.`registrationshow` = true and subscribes.year = ? and exams.exam_type_id != 4", CURRENT_YEAR_EXAM])
    @registrations = Registration.all_active
    @lasts_registrations = ActiveRecord::Base.connection.exec_query("SELECT * FROM registrations_#{LAST_YEAR_EXAM}")
    @courses = Subscribe.joins(:first_option_class, :exam).order('class_names.name').all(:select => "subscribes.`first_option_id`", :conditions=> ["subscribes.year = ?", CURRENT_YEAR_EXAM], :group => "subscribes.`first_option_id`", :order => "subscribes.`first_option_id`")

  end
end
