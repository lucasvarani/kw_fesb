class Admin::ExAlunosController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  def index
    @ex_alunos = ExAluno.all
  end

  def show
    @ex_aluno = ExAluno.find(params[:id])
  end

  def new
    @ex_aluno = ExAluno.new
  end

  def create
    @ex_aluno = ExAluno.new(params[:ex_aluno])
    if @ex_aluno.save
      redirect_to @ex_aluno, :notice => "Successfully created ex aluno."
    else
      render :action => 'new'
    end
  end

  def edit
    @ex_aluno = ExAluno.find(params[:id])
  end

  def update
    @ex_aluno = ExAluno.find(params[:id])
    if @ex_aluno.update_attributes(params[:ex_aluno])
      redirect_to @ex_aluno, :notice  => "Successfully updated ex aluno."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @ex_aluno = ExAluno.find(params[:id])
    @ex_aluno.destroy
    redirect_to ex_alunos_url, :notice => "Successfully destroyed ex aluno."
  end
end
