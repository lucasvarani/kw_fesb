class Admin::PreSubscribesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end

  layout "admin"

  def index
    @pre_subscribes = PreSubscribe.search(params[:class_name], params[:class_category], params[:data])
  end

  def show
    @pre_subscribe = PreSubscribe.find(params[:id])
  end

  def new
    @pre_subscribe = PreSubscribe.new
  end

  def create
    @pre_subscribe = PreSubscribe.new(params[:pre_subscribe])
    if @pre_subscribe.save
      redirect_to [:admin, @pre_subscribe], :notice => "Successfully created pre subscribe."
    else
      render :action => 'new'
    end
  end

  def edit
    @pre_subscribe = PreSubscribe.find(params[:id])
  end

  def update
    @pre_subscribe = PreSubscribe.find(params[:id])
    if @pre_subscribe.update_attributes(params[:pre_subscribe])
      redirect_to [:admin, @pre_subscribe], :notice  => "Successfully updated pre subscribe."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @pre_subscribe = PreSubscribe.find(params[:id])
    @pre_subscribe.newdestroy
    redirect_to admin_pre_subscribes_url, :notice => "Successfully destroyed pre subscribe."
  end
end
