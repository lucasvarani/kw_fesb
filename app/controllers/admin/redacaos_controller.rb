class Admin::RedacaosController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end

    layout "admin"

  def index
    @redacaos = Redacao.all
  end

  def show
    @redacao = Redacao.find params[:id]    
  end

end
