class Admin::EnemsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end

    layout "admin"

  def index
    @enems = Enem.all
  end

  def show
    @enem = Enem.find params[:id]    
  end

end
