class Admin::LibrarieTabNamesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :library, :all
  end

  layout "admin"

  def index
    @tabs = LibrarieTabName.all
  end

  def show
    @tab = LibrarieTabName.find(params[:id])
  end

  def new
    @tab = LibrarieTabName.new
  end

  def create
    @tab = LibrarieTabName.new(params[:librarie_tab_name])
    if @tab.save
      redirect_to [:admin, @tab], :notice => "Successfully created tab."
    else
      render :action => 'new'
    end
  end

  def edit
    @tab = LibrarieTabName.find(params[:id])
  end

  def update
    @tab = LibrarieTabName.find(params[:id])
    if @tab.update_attributes(params[:librarie_tab_name])
      redirect_to admin_librarie_tab_names_path, :notice  => "Successfully updated tab."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @tab = LibrarieTabName.find(params[:id])
    @tab.newdestroy
    redirect_to admin_categories_url, :notice => "Successfully destroyed tab."
  end
end
