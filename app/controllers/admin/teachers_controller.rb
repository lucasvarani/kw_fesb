class Admin::TeachersController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  
  def index
    @teachers = Teacher.all_active
  end

  def show
    @teacher = Teacher.find(params[:id])
  end

  def new
    @teacher = Teacher.new
  end

  def create
    @teacher = Teacher.new(params[:teacher])
    if @teacher.save
      redirect_to [:admin, @teacher], :notice => "Successfully created teacher."
    else
      render :action => 'new'
    end
  end

  def edit
    @teacher = Teacher.find(params[:id])
  end

  def update
    @teacher = Teacher.find(params[:id])
    if @teacher.update_attributes(params[:teacher])
      redirect_to [:admin, @teacher], :notice  => "Successfully updated teacher."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @teacher = Teacher.find(params[:id])
    @teacher.newdestroy
    redirect_to admin_teachers_url, :notice => "Successfully destroyed teacher."
  end
end
