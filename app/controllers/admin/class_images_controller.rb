class Admin::ClassImagesController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  before_filter :load_class
  def index
    @class_images = @class_name.class_images.all_active
  end

  def show
    @class_image = @class_name.class_images.find(params[:id])
  end

  def new
    @class_image = @class_name.class_images.build
  end

  def create
    @class_image = @class_name.class_images.build(params[:class_image])
    if @class_image.save
      redirect_to [:admin, @class_name, @class_image], :notice => "Successfully created class image."
    else
      render :action => 'new'
    end
  end

  def edit
    @class_image = @class_name.class_images.find(params[:id])
  end

  def update
    @class_image = @class_name.class_images.find(params[:id])
    if @class_image.update_attributes(params[:class_image])
      redirect_to [:admin, @class_name, @class_image], :notice  => "Successfully updated class image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @class_image = @class_name.class_images.find(params[:id])
    @class_image.newdestroy
    redirect_to admin_class_name_class_images_url, :notice => "Successfully destroyed class image."
  end
  
  def load_class
    @class_name = ClassName.find_by_id(params[:class_name_id])
  end
end
