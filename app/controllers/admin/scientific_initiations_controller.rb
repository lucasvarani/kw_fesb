class Admin::ScientificInitiationsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  
  def index
    @scientific_initiations = ScientificInitiation.all_active
  end

  def show
    @scientific_initiation = ScientificInitiation.find(params[:id])
  end

  def new
    @scientific_initiation = ScientificInitiation.new
  end

  def create
    @scientific_initiation = ScientificInitiation.new(params[:scientific_initiation])
    if @scientific_initiation.save
      redirect_to [:admin, @scientific_initiation], :notice => "Successfully created scientific initiation."
    else
      render :action => 'new'
    end
  end

  def edit
    @scientific_initiation = ScientificInitiation.find(params[:id])
  end

  def update
    @scientific_initiation = ScientificInitiation.find(params[:id])
    if @scientific_initiation.update_attributes(params[:scientific_initiation])
      redirect_to [:admin, @scientific_initiation], :notice  => "Successfully updated scientific initiation."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @scientific_initiation = ScientificInitiation.find(params[:id])
    @scientific_initiation.newdestroy
    redirect_to admin_scientific_initiations_url, :notice => "Successfully destroyed scientific initiation."
  end
end
