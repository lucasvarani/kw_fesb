class Admin::WarningsController < ApplicationController
  access_control do
      allow :administrator, :all
      allow :marketing, :all
  end
  
  layout "admin"
  before_filter :load_type, :only => [:new, :edit]
  
  def index
    @warnings = Warning.all_active("id DESC")
  end

  def show
    @warning = Warning.find(params[:id])
  end

  def new
    @warning = Warning.new
  end

  def create
    @warning = Warning.new(params[:warning])
    if @warning.save
      redirect_to [:admin, @warning], :notice => "Successfully created warning."
    else
      render :action => 'new'
    end
  end

  def edit
    @warning = Warning.find(params[:id])
  end

  def update
    @warning = Warning.find(params[:id])
    if @warning.update_attributes(params[:warning])
      redirect_to [:admin, @warning], :notice  => "Successfully updated warning."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @warning = Warning.find(params[:id])
    @warning.newdestroy
    redirect_to admin_warnings_url, :notice => "Successfully destroyed warning."
  end
  
  def load_type
    @type_warnings = TypeWarning.all_published.collect {|c| [c.name, c.id]}
  end
end
