class PreSubscribesController < ApplicationController
  before_filter :load_class
  def class_new
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    else
      @class_name = ClassName.find_by_id(params[:id])
      @pre_subscribe = PreSubscribe.new(:class_name_id=>@class_name.id)
      if @class_name.pre_subscribe == false
        redirect_to root_path
      end
   end
  end
  def create
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    else
      @pre_subscribe = PreSubscribe.new(params[:pre_subscribe])
      if @pre_subscribe.save
        @class_name = ClassName.find_by_id(@pre_subscribe.class_name_id)
        if @class_name.id == 43
          Notifier.send_pre_subscribe_clinic(@pre_subscribe).deliver
        else
          Notifier.send_pre_subscribe(@pre_subscribe).deliver
        end
        Notifier.send_pre_subscribe_user(@pre_subscribe, @class_name).deliver
        redirect_to [@pre_subscribe], :notice => "Successfully created pre_subscribe."
      else
        @class_name = ClassName.find_by_id(@pre_subscribe.class_name_id)
        render :action => 'class_new'
      end
    end
  end

  def show
    @pre_subscribe= PreSubscribe.find_by_id(params[:id])
    @class_name = ClassName.find_by_id(@pre_subscribe.class_name_id)
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    end
  end
end
