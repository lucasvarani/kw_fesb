class SocialActionsController < ApplicationController 
  before_filter :load_class    
  def index
    @category = Category.find_by_id(4, :conditions => ['published =  true and active = true'], :include => :items)  
    if @category.blank?
      redirect_to root_path
    end
  end

  def show
    @category = Category.find_by_id(4, :conditions => ['published =  true and active = true'], :include => :items)  
    if @category.blank?
      redirect_to root_path
    end
    @item = @category.items.find_by_id(params[:id])
    if @item.blank?
      redirect_to social_actions_path()
    end
  end
end
