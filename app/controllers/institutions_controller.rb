class InstitutionsController < ApplicationController
  before_filter :load_class
  def index
    @category = Category.find_by_id(1, :conditions => ['published =  true and active = true'], :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @pictures = @category.items.find_by_id(5)
    @warnings = Warning.paginate(:page => params[:page], :per_page => 8).find(:all, :conditions => ['published =  true and active = true and type_warning_id = 7'], :order => "id desc")
    @warnings_regiments = Warning.find(:all, :conditions => ['published =  true and active = true and type_warning_id = 15'], :order => "id desc")
    @warnings_standings = Warning.paginate(:page => params[:page], :per_page => 5).find(:all, :conditions => ['published =  true and active = true and type_warning_id = 16'], :order => "id desc")
  end

  def show
    redirect_to institutions_path
    # @category = Category.find_by_id(1, :conditions => ['published =  true and active = true'], :include => :items)
    # if @category.blank?
    #   redirect_to root_path
    # end
    # @item = @category.items.find_by_id(params[:id])
    # if @item.blank?
    #   redirect_to institutions_path()
    # end
  end

end
