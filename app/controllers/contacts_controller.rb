class ContactsController < ApplicationController
  before_filter :load_class
  invisible_captcha :only => :create, :on_spam => :spam_detected
  def index
    @contact = Contact.new
    @departments = Department.all_published("name").collect {|c| [c.name, c.id]}
    @work = Work.new
    @ex_aluno = ExAluno.new
  end

  def new
    redirect_to contacts_path
    # @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    # if @category.blank?
    #   redirect_to root_path
    # else
    #   @contact = Contact.new
    #   @departments = Department.all_published("name").collect {|c| [c.name, c.id]}
    # end
  end

  def create
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    else
      @contact = Contact.new(params[:contact])
      if @contact.save
        Notifier.send_contact(@contact, @contact.department.email).deliver
        Notifier.send_contact_user(@contact).deliver
        redirect_to [@contact], :notice => "Successfully created contact."
      else
        @departments = Department.all_published("name").collect {|c| [c.name, c.id]}
        render :action => 'new'
      end
    end
  end

  def show
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    end
  end

  def spam_detected
    redirect_to root_path, :alert => 'Spam detected'
  end

end
