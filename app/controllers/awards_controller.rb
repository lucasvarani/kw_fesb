class AwardsController < ApplicationController
  before_filter :load_class
  def index
    @category = Category.find_by_id(8, :conditions => ['published =  true and active = true'], :include => :items)  
    if @category.blank?
      redirect_to root_path
    end
    @awards = Award.find(:all, :conditions => ['published =  true and active = true'])
  end

  def show
    @category = Category.find_by_id(8, :conditions => ['published =  true and active = true'], :include => :items)  
    if @category.blank?
      redirect_to root_path
    end
    @awards = Award.find(:all, :conditions => ['published =  true and active = true'])
    @award = Award.find_by_id(params[:id], :conditions => ['published =  true and active = true'])
    if @award.blank?
      redirect_to awards_path
    end
  end
end
