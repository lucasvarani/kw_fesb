class RedacaosController < ApplicationController
	layout 'vagas'
	before_filter :redirect_root 
	include ActionView::Helpers::TextHelper

  def index
    @redacaos = Redacao.all("id desc")
  end

  def show
    @redacao = Redacao.find(params[:id])
    if @redacao.essay.blank?
    	redirect_to edit_redacao_path(@redacao)
    end
  end

  def new
    @redacao = Redacao.new
  end

  def create
    @redacao = Redacao.new(params[:redacao])
    if @redacao.save
      redirect_to edit_redacao_path(@redacao)
    else
      render :action => 'new'
    end
  end

  def edit
    @redacao = Redacao.find(params[:id])
    if !@redacao.essay.blank?
    	flash[:notice]  = "Redação já foi elaborada"
      redirect_to redacao_redacaos_path
    end
  end

  def update
    @redacao = Redacao.find(params[:id])
    if @redacao.update_attributes(params[:redacao])
    	Notifier.send_essay(@redacao).deliver
      # Notifier.send_essay_user(@redacao).deliver
    	filename = "#{@redacao.name}_redacao_#{@redacao.id}.pdf"

    	pdf = WickedPdf.new.pdf_from_string(simple_format(@redacao.essay))

    	save_path = Rails.root.join('public', filename)
    	File.open(save_path, 'wb') do |file|
    	  file << pdf
    	end
      redirect_to [@redacao], :notice  => "Successfully updated Redacao."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @redacao = Redacao.find(params[:id])
    @redacao.newdestroy
    redirect_to redacaos_url, :notice => "Successfully destroyed Redacao."
  end

  def load_classes
  	@class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
  	@class_names = ClassName.all(:conditions => ["published = 1 and active = 1 and class_category_id = 1 and show_remaining = 1 and id not in (?) and pre_subscribe = 0", @class_children.map{|s| s.father_id}], :order => :name)
  end

  def find_essay
  	@redacao = Redacao.find_by_cpf(params[:cpf])
    if @redacao
      if @redacao.essay.blank?
        redirect_to edit_redacao_path(@redacao)
      elsif !@redacao.essay.blank?
        flash[:notice]  = "Redação já foi elaborada"
        redirect_to redacao_redacaos_path   
      end      
  	else
      flash[:notice]  = "CPF inválido tente novamente."
      redirect_to redacao_redacaos_path
    end  	  	
  end

  def redirect_root
    redirect_to :root
  end
end
