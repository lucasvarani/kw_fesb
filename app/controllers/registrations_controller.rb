class RegistrationsController < ApplicationController
  before_filter :load_class, :load_subscribe 
  layout 'exams'
  def new
    if @subscribe.registrations.all.blank?
      @registration = @subscribe.registrations.build
    else
      redirect_to [@subscribe, @subscribe.registrations.all.first], :notice => "Você já fez sua pré matricula em #{I18n.localize(@subscribe.registrations.all.first.created_at, :format=>"%m/%d/%Y")}."
    end
  end
  
  def create
    @registration = @subscribe.registrations.build(params[:registration])
    #params[:registration][:scheduling] = params[:registration][:scheduling].to_date
    puts params[:registration][:scheduling].to_s
    if @registration.save
      Notifier.send_registration(@registration).deliver 
      redirect_to [@subscribe, @registration], :notice => "Successfully created registration."
    else
      render :action => 'new'
    end
  end
  
  def show
    @registration = Registration.find(params[:id])
    @exam_type = @registration.subscribe.exam.exam_type_id
  end
  
  def load_subscribe
    @subscribe = Subscribe.find(params[:subscribe_id])
    date_of_birth = @subscribe.user.birth_date
    as_at = Time.now
    as_at = as_at.utc.to_date if as_at.respond_to?(:utc)
    @idade = as_at.year - date_of_birth.year - ((as_at.month > date_of_birth.month || (as_at.month == date_of_birth.month && as_at.day >= date_of_birth.day)) ? 0 : 1)
  end
  
  def search_cep
    url = URI::escape("http://webservice.kinghost.net/web_cep.php?auth=3d25c66b26b656be598f026367f47e8b&formato=json&cep=#{params[:cep]}")
    @return = Net::HTTP.get(URI.parse(url)) 
    render :json => @return
  end
end
