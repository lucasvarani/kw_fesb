class ClassCategoriesController < ApplicationController
  before_filter :load_class
  def index
    redirect_to root_path
  end

  def show
    @category = Category.find_by_id(2, :conditions => ['published =  true and active = true'])
    @class_category = ClassCategory.find_by_id(params[:id], :conditions => ['published =  true and active = true'], :include => :class_names)
    if current_user
      @exam_participants = Subscribe.all(:conditions =>["user_id = ?", current_user.id])
      if @exam_participants.count > 0
        @exams = Exam.all(:conditions => ["published = 1 AND active = 1 AND start_date < ? AND end_date > ? AND id not in(#{@exam_participants.map(&:exam_id).join(',')})", DateTime.now, DateTime.now])
      end
    end
  end

  def innership_standard
    @category = Category.find_by_id(11, :include => :items)
    if @category.blank?
      redirect_to root_path
    end
    @item = @category.items.find_by_id(params[:id])


  end
end
