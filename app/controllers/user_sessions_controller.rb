class UserSessionsController < ApplicationController
  before_filter :load_class
  layout "vagas"
  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      if ((User.find(current_user.id).has_role? :administrator) || (User.find(current_user.id).has_role? :marketing) || (User.find(current_user.id).has_role? :library) || (User.find(current_user.id).has_role? :management))
        redirect_to admin_root_path
      else
        redirect_to "/vestibular/login"
      end
    else
      flash[:error] = true
      render login_exams_path
    end
  end

  def destroy
    @user_session = UserSession.find(params[:id])
    @user_session.destroy
    redirect_to "/vestibular", :notice => "Logout efetuado com sucesso"
  end
end
