class EnemsController < ApplicationController
	layout 'vagas'
	before_filter :load_classes

  def show
    @enem = Enem.find(params[:id])
  end

  def new
    @enem = Enem.new
  end

  def create
    @enem = Enem.new(params[:enem])
    if @enem.save
      Notifier.send_enem(@enem).deliver
      Notifier.send_enem_user(@enem).deliver
      redirect_to [@enem]
    else
      render :action => 'new'
    end
  end  

  def load_classes
  	@class_children = ClassName.all(:select => "father_id",:conditions => ["published = 1 and active = 1 and class_category_id = 1 and father_id is not null and pre_subscribe = 0"], :group => "father_id")
  	@class_names = ClassName.all(:conditions => ["published = 1 and active = 1 and class_category_id = 1 and show_remaining = 1 and id not in (?) and pre_subscribe = 0", @class_children.map{|s| s.father_id}], :order => :name)
  end
  
end