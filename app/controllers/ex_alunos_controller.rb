class ExAlunosController < ApplicationController
  before_filter :load_class
  invisible_captcha :only => :create, :on_spam => :spam_detected
  def new
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])
    if @category.blank?
      redirect_to root_path
    else
      @ex_aluno = ExAluno.new
    end
  end

  def create
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true']) 
    if @category.blank?
      redirect_to root_path
    else
      @ex_aluno = ExAluno.new(params[:ex_aluno])
      # if simple_captcha_valid?
      # puts "foi --------------------------------------------------------------------"
      if @ex_aluno.save
        Notifier.send_contact_exaluno(@ex_aluno).deliver 
        Notifier.send_contact_user_exaluno(@ex_aluno).deliver
        redirect_to @ex_aluno, :notice => "Successfully created ex aluno."
      else
        render :action => 'new'
      end
      # else

      #   puts "num foi --------------------------------------------------------------------"
      #   flash[:error] = true
      #   render :action => 'new'
      # end
    end
  end

  def show
    @category = Category.find_by_id(6, :conditions => ['published =  true and active = true'])  
    if @category.blank?
      redirect_to root_path
    end
  end

  def spam_detected
    redirect_to root_path, :alert => 'Spam detected'
  end

end
