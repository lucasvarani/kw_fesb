if ($('#newsSlide')){
  $(document).ready(function(){
    $('.multiple-items').slick({
      slidesToShow: 4,
      slidesToScroll: 4,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  });
}

function getCurso(idCurso) {
	console.log("teste");
	$.ajax({
		url: 'curso-'+idCurso+'.html',
		type: 'GET',
		data: '',
		success:function(i){
			$(".data-curso").html(i);
		}
	});


}

$(function(){

	$("#topo").scrollToFixed({
		limit: $("footer").offset().top-70,
	});

	$(".espiao").scrollToFixed({
		limit: $("footer").offset().top-10,
		marginTop: 100,
	});


	//$("#slide").cycle({
	//	pager: '#pager',
	//	width: null,
	//});
	//$("#slide_mid").cycle({
	//
	//});


	$("#menu-principal li").bind({
		mouseleave:function(){
			$(this).find("ul:first").hide();
			$(this).removeClass("active");
		},
		mouseenter: function(){
			$(this).addClass("active");
			$(this).find("ul:first").slideToggle("fast");
		}
	});
});

$(".scroll").click(function(event){
    event.preventDefault();
    $('html,body').animate({scrollTop:$(this.hash).offset().top - 100}, 500);
});

$(".fancybox").fancybox({
	wrapCSS    : 'fancybox-custom',
	closeClick : true,

	openEffect : 'none',

	helpers : {
		title : {
			type : 'inside'
		},
		overlay : {
			css : {
				'background' : 'rgba(238,238,238,0.85)'
			}
		}
	}
});


/**
$(".fancybox").fancybox({
	'titlePosition' 		: 'inside'
});
**/
if (screen.width>767){
  $(document).ready(function() {
    $('.lista-lateral').scrollToFixed({
        marginTop: function() {
            return $('.site-navbar').outerHeight(true) + 10;
        },
        limit: function() {
            return (
                $('.footer').offset().top -
                $('.lista-lateral').outerHeight(true) -
                55
            );
        },
        removeOffsets: true
    });
  });
}

$(function() {
  $(".pagination a").live("click", function() {
    $(".pagination").html("Page is loading...");
    $.getScript(this.href);
    return false;
  });
});

$('.pagination .translation_missing').text("");
$('.pagination .current').text("");
$('.pagination a').text("");

$(document).ready(function(){
  $('.select2').select2();
});

jQuery.fn.center = function () {
  this.css("position","absolute");
  this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                                              $(window).scrollTop()) + "px");
  this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                              $(window).scrollLeft()) + "px");
  return this;
}

$(document).ready(function(){
  $("form").on("submit", function(){
    $("body").css("overflow", "hidden");
    $("#sendloader").fadeIn();
    $("#sendloader").center();
  });//submit
});//document ready

// $("#overlay").center();
// $(window).load(function () {
//   // Page is fully loaded .. time to fade out your div with a timer ..
//   $('#overlay').fadeOut(1000);
//   $("body").removeClass('overflowhiden');
// });