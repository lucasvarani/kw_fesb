// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs


$(document).ready(function(){
  $('.select2').select2();
  $.fn.dataTable.moment( 'DD/MM/YYYY' );
  if ($('.dataTable')) {
	$('.dataTable').DataTable({
  		"aaSorting": []
  	});
  }
  if ($('#classTable')) {
  	$('#classTable').DataTable({
  	  "info":    false,
  	  responsive: true,
  	  "lengthChange": false,
  	  "searching": false,
  	  "pageLength": 5
  	});
  }
  

});

$(window).load(function () {
  // Page is fully loaded .. time to fade out your div with a timer ..
  $('#overlay').fadeOut(1000);
});



$(document).ready(function(){
  $("form").on("submit", function(){
    // $("body").css("overflow", "hidden");
    $("#sendloader").fadeIn();
  });//submit
});//document ready