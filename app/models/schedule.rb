class Schedule < ActiveRecord::Base
  attr_accessible :name, :schedule_date, :schedule_date_end, :schedule_time, :description, :published, :active, :attach, :attach_file_name
  has_attached_file :attach
end
