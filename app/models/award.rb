class Award < ActiveRecord::Base
  attr_accessible :name, :description, :active, :published, :award_image
  
  has_attached_file :award_image,
                      :styles =>{
                        :thumb=> "50x50#",                        
                        :big_thumb=>"100x100#",
                        :thumb_show => "135x104#"
                        } 
                        
                        
  has_many :award_galleries 
  def url_slug()
    "#{id}-#{name.parameterize}"
  end
end
