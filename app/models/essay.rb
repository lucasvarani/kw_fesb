class Essay < ActiveRecord::Base
  attr_accessible :address, :cell, :cpf, :email, :essay, :name, :curso

  validates_presence_of :cell
  validates_presence_of :cpf
  validates_presence_of :email
  validates_presence_of :name
  validates_presence_of :curso

  validates_uniqueness_of :cpf
  
end
