class ExAluno < ActiveRecord::Base
	attr_accessible :name, :email, :subject, :phone, :message, :curso, :conclusion_year, :work_area, :w_area
  # attr_accessible :name, :email, :subject, :phone, :message, :curso, :conclusion_year, :work_area, :w_area,:captcha, :captcha_key
  validates_presence_of :name, :curso, :conclusion_year, :subject, :message
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :on => :create }
  # apply_simple_captcha
  # validates :name, :invisible_captcha => true
end
