class AwardGallery < ActiveRecord::Base
  attr_accessible :name, :active, :published, :award_img, :award_id
  
  has_attached_file :award_img,
                      :styles =>{
                        :thumb=> "50x50#",
                        :big_thumb=>"100x100#",
                        :thumb_show => "135x104#"
                        } 
                        
  belongs_to :award
  
  
end
