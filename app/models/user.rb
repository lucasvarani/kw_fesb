class User < ActiveRecord::Base
  attr_accessible :name,
                  :social_name,
                  :email,
                  :password,
                  :password_confirmation,
                  :role,
                  :civil_state,
                  :special_needs_phisics,
                  :high_school,
                  :special_needs_hearing,
                  :cell_phone,
                  :special_needs_visual,
                  :phone,
                  :birth_date,
                  :cpf,
                  :rg,
                  :rne,
                  :rg_oe,
                  :rg_ee,
                  :place_of_birth,
                  :sex,
                  :special_needs,
                  :ex_student,
                  :address,
                  :number,
                  :complement,
                  :neighbourhood,
                  :state,
                  :postal_code,
                  :city,
                  :first_option_id,
                  :second_option_id,
                  :cms_user,
                  :nota_enem

  validates_presence_of :name
  validates_presence_of :email
  # validates_presence_of :civil_state,  :if => :valida_user?
  # validates_presence_of :high_school,  :if => :valida_user?
  validates_presence_of :cell_phone ,  :if => :valida_user?
  # validates_presence_of :phone,  :if => :valida_user?
  validates_presence_of :birth_date,  :if => :valida_user?
  validates_presence_of :cpf,  :if => :valida_user?
  validates_presence_of :rg,  :if => :valida_user?
  # validates_presence_of :rg_oe,  :if => :valida_user?
  # validates_presence_of :rg_ee,  :if => :valida_user?
  # validates_presence_of :place_of_birth,  :if => :valida_user?
  # validates_presence_of :sex, :if => :valida_user?
  validates_presence_of :address,  :if => :valida_user?
  validates_presence_of :number,  :if => :valida_user?
  validates_presence_of :neighbourhood,  :if => :valida_user?
  validates_presence_of :state,  :if => :valida_user?
  validates_presence_of :postal_code,  :if => :valida_user?
  validates_presence_of :city, :if => :valida_user?
  validates_presence_of :first_option_id, :if => :valida_user?
  validates_presence_of :second_option_id, :if => :valida_user?
  validates_uniqueness_of :cpf, :if => :valida_user?
  attr_accessor :role
  #has_many :teams
  #has_many :collaborators, :through => :teams, :source => :collaborator
  #has_and_belongs_to_many :collaborators, :class_name => "User", :join_table => "collaborators_users", :foreign_key => "user_id", :association_foreign_key => "collaborator_id"

  #has_many :proposals
  #has_many :contacts
  #has_many :goals_sellers
  # validate :check_cpf

  acts_as_authentic do |c|
    c.login_field = "email"
  end

  acts_as_authorization_subject :association_name => :roles, :join_table_name => :roles_users
  after_save :define_role

  belongs_to :first_option_class, :class_name => "ClassName", :foreign_key => "first_option_id"
  belongs_to :second_option_class, :class_name => "ClassName", :foreign_key => "second_option_id"
  has_many :subscribes
  has_many :file_imports
  #has_many :users

  # def check_cpf(cpf=nil)
  #   return false if cpf.nil?

  #   nulos = %w{12345678909 11111111111 22222222222 33333333333 44444444444 55555555555 66666666666 77777777777 88888888888 99999999999 00000000000}
  #   valor = cpf.scan /[0-9]/
  #   if valor.length == 11
  #     unless nulos.member?(valor.join)
  #       valor = valor.collect{|x| x.to_i}
  #       soma = 10*valor[0]+9*valor[1]+8*valor[2]+7*valor[3]+6*valor[4]+5*valor[5]+4*valor[6]+3*valor[7]+2*valor[8]
  #       soma = soma - (11 * (soma/11))
  #       resultado1 = (soma == 0 or soma == 1) ? 0 : 11 - soma
  #       if resultado1 == valor[9]
  #         soma = valor[0]*11+valor[1]*10+valor[2]*9+valor[3]*8+valor[4]*7+valor[5]*6+valor[6]*5+valor[7]*4+valor[8]*3+valor[9]*2
  #         soma = soma - (11 * (soma/11))
  #         resultado2 = (soma == 0 or soma == 1) ? 0 : 11 - soma
  #         return true if resultado2 == valor[10] # CPF válido
  #       end
  #     end
  #   end
  #   return false # CPF inválido
  # end

  def valida_user?
    cms_user == true
  end

  def role
    @role
    if self.has_role? :administrator
      @role = :administrator
    elsif self.has_role? :marketing
      @role = :marketing
    elsif self.has_role? :library
      @role = :library
    elsif self.has_role? :management
      @role = :management
    else self.has_role? :user
      @role = :user
    end
  end

  def define_role
    if @role
      self.has_no_roles!
      self.has_role! @role
    end
  end

  def self.all_cms_users
    joins(:roles).where('roles.name != ?', 'user')
  end

  def deliver_password_reset_instructions!
    reset_perishable_token!
  end

end
