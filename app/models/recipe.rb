class Recipe < ActiveRecord::Base
  attr_accessible :title, :description, :recipe_img
  
  has_attached_file :recipe_img,
                      :styles =>{
                        :thumb=> "50x50#",
                        :lateral=> "390x390>"
                        } 
end
