class Registration < ActiveRecord::Base
  attr_accessible :subscribe_id, :name, :cpf, :address, :postal_code, :number, :complement, :neighbourhood, :state, :city, :active, :published, :scheduling, :phone, :arquivo_rg, :arquivo_rg_verso, :comprovante_enem

  belongs_to :subscribe

  has_attached_file :comprovante_enem
  has_attached_file :arquivo_rg
  has_attached_file :arquivo_rg_verso

  validates_presence_of :name
  validates_presence_of :cpf
  validates_presence_of :address
  validates_presence_of :postal_code
  validates_presence_of :number
  validates_presence_of :neighbourhood
  validates_presence_of :state
  validates_presence_of :city
  validates_presence_of :scheduling
  validates_presence_of :phone

  # validates_attachment_presence :comprovante_enem
  validates_attachment_presence :arquivo_rg
  validates_attachment_presence :arquivo_rg_verso

end
