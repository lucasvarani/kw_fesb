class Image < ActiveRecord::Base
  # attr_accessible :title, :body
  has_attached_file :file
  attr_accessible :file, :alt, :hint
end
