class Teacher < ActiveRecord::Base
  attr_accessible :name, :curriculum_vitae, :active, :published, :teacher_image
  has_attached_file :teacher_image,
                    :styles =>{
                    :thumb=> "50x50#"
                    }
  has_many :teaching_bodies 
  has_many :class_names,  :through => :teaching_bodies
  has_many :class_names
  
    
end
