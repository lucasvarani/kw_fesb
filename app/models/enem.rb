class Enem < ActiveRecord::Base
  attr_accessible :cell, :cpf, :curso, :name, :nota, :email

  validates_presence_of :cell
  validates_presence_of :cpf
  validates_presence_of :email
  validates_presence_of :name
  validates_presence_of :curso
  validates_presence_of :nota

  validates_uniqueness_of :cpf
end
