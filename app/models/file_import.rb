class FileImport < ActiveRecord::Base
  attr_accessible :log, :error, :user_id, :fileimport
  has_attached_file :fileimport
  belongs_to :user
  validates_attachment_presence :fileimport
end
