class Exam < ActiveRecord::Base
  attr_accessible :name, :exam_type_id, :description, :start_date, :end_date, :published, :active, :view_notes, :year
  validates_presence_of :name, :exam_type_id, :start_date, :end_date

  has_many :exam_dates 
  has_many :subscribes
end
