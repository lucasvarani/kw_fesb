class ItinerantExam < ActiveRecord::Base
  attr_accessible :city, :phone, :address
  has_many :exam_dates
end
