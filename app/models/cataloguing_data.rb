class CataloguingData < ActiveRecord::Base
  attr_accessible :name, :email, :ra, :author, :advisor, :title_subtitle, :course, :number_pages, :number_illustrations, :keywords, :additional_information
end
