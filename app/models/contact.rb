class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :phone, :department_id, :message, :active, :published 
  validates_presence_of :name, :department_id, :message
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :on => :create }     
  belongs_to :department
end
