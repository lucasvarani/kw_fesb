class Work < ActiveRecord::Base
  attr_accessible :name, :email, :phone, :department_id, :message, :active, :published, :attachment_work
  has_attached_file :attachment_work
  
  validates_presence_of :name, :email, :message
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :on => :create }
  
  belongs_to :department
end
