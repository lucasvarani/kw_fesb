class PreSubscribe < ActiveRecord::Base
  attr_accessible :class_name_id, :name, :social_name, :email, :phone, :message, :active, :published, :rg, :cpf, :graduate_class, :graduate_year, :graduate_institution, :birthday


  validates_presence_of :name, :email, :rg, :cpf, :phone
  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, :on => :create }


  belongs_to :class_name

  def self.search(class_category, class_name, data, exceptions = nil)
    subscribe = PreSubscribe.where(:active => true)
    name = class_category
    category = class_name
    date = data
    if category.present? and name == "" and date == ""
      # binding.pry
      search = subscribe.where("EXISTS(select 1 from class_categories inner join class_names where class_categories.name = '#{category}' and class_categories.id in(class_names.class_category_id) and class_names.id in(pre_subscribes.class_name_id))")
      return search
    elsif category.present? and name.present? and date == ""
      search = subscribe.where("EXISTS(select 1 from class_categories inner join class_names where class_categories.name = '#{category}' and class_categories.id in(class_names.class_category_id) and class_names.id in(pre_subscribes.class_name_id) and class_names.name LIKE ?)", "%#{name}%").uniq
      return search
    elsif category.present?
      search = subscribe.where("EXISTS(select 1 from class_categories inner join class_names where class_categories.name = '#{category}' and class_categories.id in(class_names.class_category_id) and class_names.id in(pre_subscribes.class_name_id) and class_names.name LIKE ?) AND EXTRACT(year FROM created_at) LIKE ?", "%#{name}%", "%#{date}%") if name.present?
      search = subscribe.where("EXISTS(select 1 from class_categories inner join class_names where class_categories.name = '#{category}' and class_categories.id in(class_names.class_category_id) and class_names.id in(pre_subscribes.class_name_id)) AND EXTRACT(year FROM pre_subscribes.created_at) LIKE ?", "%#{date}%") if name == ""
      return search
    elsif date.present? and name == "" and category = ""
      search = subscribe.where("EXTRACT(year FROM created_at) LIKE ?", "%#{date}%")
      return search
    else
      return subscribe
    end
  end

end
