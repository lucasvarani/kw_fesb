class ItemImage < ActiveRecord::Base
  attr_accessible :item_id, :name, :active, :published, :item_img
  
    has_attached_file :item_img,
                      :styles =>{
                        :thumb=> "50x50#",
                        :big_thumb=>"100x100#",
                        :thumb_show => "135x104#"
                        }
  belongs_to :item
  
end
