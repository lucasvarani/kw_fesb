class Warning < ActiveRecord::Base
  attr_accessible :type_warning_id, :name, :short_description, :description, :active, :published, :show_home, :warning_image, :warning_file, :is_library,:attachments_array
  validates_presence_of :type_warning_id
  has_attached_file :warning_image,
                      :styles =>{
                      :thumb=> "50x50#",
                      :thumb_home=> "193x113#"
                      } 
  has_attached_file :warning_file
  belongs_to :type_warning
  has_many :warning_images 
  
  def url_slug()
    "#{id}-#{name.parameterize}"
  end
  
  def attachments_array=(array)
   array.each do |file|
     warning_images.build(:warning_images => file)
   end
  end
end
