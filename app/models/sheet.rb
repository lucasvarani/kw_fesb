class Sheet < ActiveRecord::Base
  attr_accessible :name, :email, :ra, :author, :advisor, :title_subtitle, :course, :number_pages, :number_illustrations, :keywords, :additional_information, :class_name_id, :code_name
  validates_presence_of :name, :email, :ra, :author, :advisor, :title_subtitle, :number_pages, :number_illustrations, :keywords, :class_name_id
  
  belongs_to :class_name
end
