class TypeWarning < ActiveRecord::Base
  attr_accessible :name, :active, :published
  
  has_many :warnings 
  def url_slug()
    "#{id}-#{name.parameterize}"
  end
end
