class ClassStyle < ActiveRecord::Base
  attr_accessible :name, :active, :published
  has_many :class_names
end
