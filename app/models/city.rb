class City < ActiveRecord::Base
  attr_accessible :state, :title
  has_many :socioeconomics
end
