class ClassCategory < ActiveRecord::Base
  attr_accessible :name, :active, :published
  
  has_many :class_names   
  
  def url_slug()
    "#{id}-#{name.parameterize}"
  end
end
