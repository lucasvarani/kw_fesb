class ClassImage < ActiveRecord::Base
  attr_accessible :class_name_id, :name, :active, :published, :class_images
  has_attached_file :class_images,
                    :styles =>{
                      :thumb=> "50x50#", 
                      :thumb_home => "135x104#"
                      }  
                      
  belongs_to :class_name
end
