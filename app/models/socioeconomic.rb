class Socioeconomic < ActiveRecord::Base
  attr_accessible :birth_state, :birth_city, :income, :income_family, :school, :highschool, :occupation, :how_did_me, :best_ad, :city_id
  # validates_presence_of :income, :income_family, :school, :highschool, :occupation, :how_did_me, :best_ad
  validates_presence_of :how_did_me, :best_ad
  has_many :subscribes
  belongs_to :city
end
