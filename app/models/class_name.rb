class ClassName < ActiveRecord::Base

  attr_accessible :class_category_id, :name, :duration, :period, :number_of_vacancies, :description, :labour_market, :active, :published, :class_icon, :class_header, :class_footer, :teacher_ids, :teacher_id, :class_matrix, :class_style_id, :objective, :differential, :target, :requirements, :pre_subscribe, :cdd, :embed, :father_id, :show_subscribe, :show_remaining, :pha, :summary_content, :price, :financing_6, :financing_8, :financing_10

    has_attached_file :class_icon,
                      :styles =>{
                        :thumb=> "50x50#"
                        }
    has_attached_file :class_header,
                      :styles =>{
                        :thumb=> "50x50#"
                        }
    has_attached_file :class_footer,
                      :styles =>{
                        :thumb=> "50x50#"
                        }
    has_attached_file :class_matrix
    has_attached_file :summary_content
   has_many :class_images
   belongs_to :class_category
   belongs_to :class_style
   has_many :teaching_bodies
   belongs_to :teacher
   has_many:teachers,  :through => :teaching_bodies
   has_many :users
   has_many :subscribes
   has_many :pre_subscribes
   has_many :sheets


   has_many :children, :class_name => "ClassName", :foreign_key => "father_id"
   belongs_to :father, :class_name => "ClassName"

   def url_slug()
     "#{id}-#{name.parameterize}"
   end
end
