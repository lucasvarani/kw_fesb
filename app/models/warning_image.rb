class WarningImage < ActiveRecord::Base
  attr_accessible :warning_id, :name, :active, :published, :warning_images
  has_attached_file :warning_images,
                      :styles =>{
                      :thumb=> "50x50#",
                      :big_thumb=>"100x100#",
                      :thumb_show => "135x104#"
                      } 
  belongs_to :warning
  
end
