class Item < ActiveRecord::Base
  attr_accessible :name, :description, :active, :published, :item_file
  has_attached_file :item_file
  
  belongs_to :category
  has_many :item_images 
  def url_slug()
    "#{id}-#{name.parameterize}"
  end
end
