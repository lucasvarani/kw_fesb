class Subscribe < ActiveRecord::Base
  attr_accessible :exam_id, :socioeconomic_id, :user_id, :price, :ticket_number, :first_option_id, :second_option_id, :registration, :paid, :copywriting_note, :note_issues, :copywriting_weight, :weight_issues, :endnote, :attended, :exam_date_id, :registrationshow, :confirmation
  belongs_to :exam_date
  belongs_to :exam
  belongs_to :user
  belongs_to :first_option_class, :class_name => "ClassName", :foreign_key => "first_option_id"
  belongs_to :second_option_class, :class_name => "ClassName", :foreign_key => "second_option_id"
  belongs_to :socioeconomic, :dependent => :destroy
  has_many :registrations


end
