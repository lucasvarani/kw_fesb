class ScientificInitiation < ActiveRecord::Base
  attr_accessible :name, :active, :published, :file
  
  has_attached_file :file
end
