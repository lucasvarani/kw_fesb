class Highlight < ActiveRecord::Base
  attr_accessible :name, :flag, :link, :target, :active, :published, :high_img, :highlight_file
  
  has_attached_file :high_img,
                      :styles =>{
                        :thumb=> "50x50#",
                        :lateral=> "390x390>"
                        } 
  has_attached_file :highlight_file
end
