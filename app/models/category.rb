class Category < ActiveRecord::Base
  attr_accessible :name, :active , :published, :category_img
    
  has_attached_file :category_img,
                    :styles =>{
                      :thumb=> "50x50#"
                      }
  has_many :items
end
