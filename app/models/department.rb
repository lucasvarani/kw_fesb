class Department < ActiveRecord::Base
  attr_accessible :name, :email, :active, :published
  
  has_many :contacts
  has_many :works
end
