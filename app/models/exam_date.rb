class ExamDate < ActiveRecord::Base
  attr_accessible :exam_date, :exam_id, :published, :active, :view_notes, :exam_city, :itinerant_exam_id
  validates_presence_of :exam_date, :exam_id
  belongs_to :exam
  has_many :subscribes
  belongs_to :itinerant_exam
end
