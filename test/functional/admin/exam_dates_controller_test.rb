require 'test_helper'

class Admin::ExamDatesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ExamDate.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ExamDate.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ExamDate.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_exam_date_url(assigns(:exam_date))
  end

  def test_edit
    get :edit, :id => ExamDate.first
    assert_template 'edit'
  end

  def test_update_invalid
    ExamDate.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ExamDate.first
    assert_template 'edit'
  end

  def test_update_valid
    ExamDate.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ExamDate.first
    assert_redirected_to admin_exam_date_url(assigns(:exam_date))
  end

  def test_destroy
    exam_date = ExamDate.first
    delete :destroy, :id => exam_date
    assert_redirected_to admin_exam_dates_url
    assert !ExamDate.exists?(exam_date.id)
  end
end
