require 'test_helper'

class Admin::SubscribesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Subscribe.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Subscribe.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Subscribe.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_subscribe_url(assigns(:subscribe))
  end

  def test_edit
    get :edit, :id => Subscribe.first
    assert_template 'edit'
  end

  def test_update_invalid
    Subscribe.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Subscribe.first
    assert_template 'edit'
  end

  def test_update_valid
    Subscribe.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Subscribe.first
    assert_redirected_to admin_subscribe_url(assigns(:subscribe))
  end

  def test_destroy
    subscribe = Subscribe.first
    delete :destroy, :id => subscribe
    assert_redirected_to admin_subscribes_url
    assert !Subscribe.exists?(subscribe.id)
  end
end
