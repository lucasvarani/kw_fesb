require 'test_helper'

class Admin::OportunitiesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Oportunity.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Oportunity.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Oportunity.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_oportunity_url(assigns(:oportunity))
  end

  def test_edit
    get :edit, :id => Oportunity.first
    assert_template 'edit'
  end

  def test_update_invalid
    Oportunity.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Oportunity.first
    assert_template 'edit'
  end

  def test_update_valid
    Oportunity.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Oportunity.first
    assert_redirected_to admin_oportunity_url(assigns(:oportunity))
  end

  def test_destroy
    oportunity = Oportunity.first
    delete :destroy, :id => oportunity
    assert_redirected_to admin_oportunities_url
    assert !Oportunity.exists?(oportunity.id)
  end
end
