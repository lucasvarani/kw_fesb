require 'test_helper'

class Admin::SheetsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Sheet.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Sheet.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Sheet.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_sheet_url(assigns(:sheet))
  end

  def test_edit
    get :edit, :id => Sheet.first
    assert_template 'edit'
  end

  def test_update_invalid
    Sheet.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Sheet.first
    assert_template 'edit'
  end

  def test_update_valid
    Sheet.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Sheet.first
    assert_redirected_to admin_sheet_url(assigns(:sheet))
  end

  def test_destroy
    sheet = Sheet.first
    delete :destroy, :id => sheet
    assert_redirected_to admin_sheets_url
    assert !Sheet.exists?(sheet.id)
  end
end
