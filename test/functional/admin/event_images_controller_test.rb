require 'test_helper'

class Admin::EventImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => EventImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    EventImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    EventImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_event_image_url(assigns(:event_image))
  end

  def test_edit
    get :edit, :id => EventImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    EventImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => EventImage.first
    assert_template 'edit'
  end

  def test_update_valid
    EventImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => EventImage.first
    assert_redirected_to admin_event_image_url(assigns(:event_image))
  end

  def test_destroy
    event_image = EventImage.first
    delete :destroy, :id => event_image
    assert_redirected_to admin_event_images_url
    assert !EventImage.exists?(event_image.id)
  end
end
