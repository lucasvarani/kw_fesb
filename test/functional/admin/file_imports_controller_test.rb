require 'test_helper'

class Admin::FileImportsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => FileImport.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    FileImport.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    FileImport.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_file_import_url(assigns(:file_import))
  end

  def test_edit
    get :edit, :id => FileImport.first
    assert_template 'edit'
  end

  def test_update_invalid
    FileImport.any_instance.stubs(:valid?).returns(false)
    put :update, :id => FileImport.first
    assert_template 'edit'
  end

  def test_update_valid
    FileImport.any_instance.stubs(:valid?).returns(true)
    put :update, :id => FileImport.first
    assert_redirected_to admin_file_import_url(assigns(:file_import))
  end

  def test_destroy
    file_import = FileImport.first
    delete :destroy, :id => file_import
    assert_redirected_to admin_file_imports_url
    assert !FileImport.exists?(file_import.id)
  end
end
