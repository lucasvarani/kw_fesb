require 'test_helper'

class Admin::ItemImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ItemImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ItemImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ItemImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_item_image_url(assigns(:item_image))
  end

  def test_edit
    get :edit, :id => ItemImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    ItemImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ItemImage.first
    assert_template 'edit'
  end

  def test_update_valid
    ItemImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ItemImage.first
    assert_redirected_to admin_item_image_url(assigns(:item_image))
  end

  def test_destroy
    item_image = ItemImage.first
    delete :destroy, :id => item_image
    assert_redirected_to admin_item_images_url
    assert !ItemImage.exists?(item_image.id)
  end
end
