require 'test_helper'

class Admin::PreSubscribesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => PreSubscribe.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    PreSubscribe.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    PreSubscribe.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_pre_subscribe_url(assigns(:pre_subscribe))
  end

  def test_edit
    get :edit, :id => PreSubscribe.first
    assert_template 'edit'
  end

  def test_update_invalid
    PreSubscribe.any_instance.stubs(:valid?).returns(false)
    put :update, :id => PreSubscribe.first
    assert_template 'edit'
  end

  def test_update_valid
    PreSubscribe.any_instance.stubs(:valid?).returns(true)
    put :update, :id => PreSubscribe.first
    assert_redirected_to admin_pre_subscribe_url(assigns(:pre_subscribe))
  end

  def test_destroy
    pre_subscribe = PreSubscribe.first
    delete :destroy, :id => pre_subscribe
    assert_redirected_to admin_pre_subscribes_url
    assert !PreSubscribe.exists?(pre_subscribe.id)
  end
end
