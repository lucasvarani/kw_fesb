require 'test_helper'

class Admin::AwardGalleriesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => AwardGallery.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    AwardGallery.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    AwardGallery.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_award_gallery_url(assigns(:award_gallery))
  end

  def test_edit
    get :edit, :id => AwardGallery.first
    assert_template 'edit'
  end

  def test_update_invalid
    AwardGallery.any_instance.stubs(:valid?).returns(false)
    put :update, :id => AwardGallery.first
    assert_template 'edit'
  end

  def test_update_valid
    AwardGallery.any_instance.stubs(:valid?).returns(true)
    put :update, :id => AwardGallery.first
    assert_redirected_to admin_award_gallery_url(assigns(:award_gallery))
  end

  def test_destroy
    award_gallery = AwardGallery.first
    delete :destroy, :id => award_gallery
    assert_redirected_to admin_award_galleries_url
    assert !AwardGallery.exists?(award_gallery.id)
  end
end
