require 'test_helper'

class Admin::ClassNamesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ClassName.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ClassName.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ClassName.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_class_name_url(assigns(:class_name))
  end

  def test_edit
    get :edit, :id => ClassName.first
    assert_template 'edit'
  end

  def test_update_invalid
    ClassName.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ClassName.first
    assert_template 'edit'
  end

  def test_update_valid
    ClassName.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ClassName.first
    assert_redirected_to admin_class_name_url(assigns(:class_name))
  end

  def test_destroy
    class_name = ClassName.first
    delete :destroy, :id => class_name
    assert_redirected_to admin_class_names_url
    assert !ClassName.exists?(class_name.id)
  end
end
