require 'test_helper'

class Admin::ClassStylesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ClassStyle.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ClassStyle.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ClassStyle.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_class_style_url(assigns(:class_style))
  end

  def test_edit
    get :edit, :id => ClassStyle.first
    assert_template 'edit'
  end

  def test_update_invalid
    ClassStyle.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ClassStyle.first
    assert_template 'edit'
  end

  def test_update_valid
    ClassStyle.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ClassStyle.first
    assert_redirected_to admin_class_style_url(assigns(:class_style))
  end

  def test_destroy
    class_style = ClassStyle.first
    delete :destroy, :id => class_style
    assert_redirected_to admin_class_styles_url
    assert !ClassStyle.exists?(class_style.id)
  end
end
