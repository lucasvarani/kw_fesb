require 'test_helper'

class Admin::ClassImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ClassImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ClassImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ClassImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_class_image_url(assigns(:class_image))
  end

  def test_edit
    get :edit, :id => ClassImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    ClassImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ClassImage.first
    assert_template 'edit'
  end

  def test_update_valid
    ClassImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ClassImage.first
    assert_redirected_to admin_class_image_url(assigns(:class_image))
  end

  def test_destroy
    class_image = ClassImage.first
    delete :destroy, :id => class_image
    assert_redirected_to admin_class_images_url
    assert !ClassImage.exists?(class_image.id)
  end
end
