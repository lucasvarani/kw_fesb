require 'test_helper'

class Admin::WarningImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => WarningImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    WarningImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    WarningImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_warning_image_url(assigns(:warning_image))
  end

  def test_edit
    get :edit, :id => WarningImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    WarningImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => WarningImage.first
    assert_template 'edit'
  end

  def test_update_valid
    WarningImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => WarningImage.first
    assert_redirected_to admin_warning_image_url(assigns(:warning_image))
  end

  def test_destroy
    warning_image = WarningImage.first
    delete :destroy, :id => warning_image
    assert_redirected_to admin_warning_images_url
    assert !WarningImage.exists?(warning_image.id)
  end
end
