require 'test_helper'

class Admin::FindsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Find.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Find.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Find.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_find_url(assigns(:find))
  end

  def test_edit
    get :edit, :id => Find.first
    assert_template 'edit'
  end

  def test_update_invalid
    Find.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Find.first
    assert_template 'edit'
  end

  def test_update_valid
    Find.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Find.first
    assert_redirected_to admin_find_url(assigns(:find))
  end

  def test_destroy
    find = Find.first
    delete :destroy, :id => find
    assert_redirected_to admin_finds_url
    assert !Find.exists?(find.id)
  end
end
