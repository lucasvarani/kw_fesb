require 'test_helper'

class Admin::FindItsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => FindIt.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    FindIt.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    FindIt.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_find_it_url(assigns(:find_it))
  end

  def test_edit
    get :edit, :id => FindIt.first
    assert_template 'edit'
  end

  def test_update_invalid
    FindIt.any_instance.stubs(:valid?).returns(false)
    put :update, :id => FindIt.first
    assert_template 'edit'
  end

  def test_update_valid
    FindIt.any_instance.stubs(:valid?).returns(true)
    put :update, :id => FindIt.first
    assert_redirected_to admin_find_it_url(assigns(:find_it))
  end

  def test_destroy
    find_it = FindIt.first
    delete :destroy, :id => find_it
    assert_redirected_to admin_find_its_url
    assert !FindIt.exists?(find_it.id)
  end
end
