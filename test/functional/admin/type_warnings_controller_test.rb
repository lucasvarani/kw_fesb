require 'test_helper'

class Admin::TypeWarningsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => TypeWarning.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    TypeWarning.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    TypeWarning.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_type_warning_url(assigns(:type_warning))
  end

  def test_edit
    get :edit, :id => TypeWarning.first
    assert_template 'edit'
  end

  def test_update_invalid
    TypeWarning.any_instance.stubs(:valid?).returns(false)
    put :update, :id => TypeWarning.first
    assert_template 'edit'
  end

  def test_update_valid
    TypeWarning.any_instance.stubs(:valid?).returns(true)
    put :update, :id => TypeWarning.first
    assert_redirected_to admin_type_warning_url(assigns(:type_warning))
  end

  def test_destroy
    type_warning = TypeWarning.first
    delete :destroy, :id => type_warning
    assert_redirected_to admin_type_warnings_url
    assert !TypeWarning.exists?(type_warning.id)
  end
end
