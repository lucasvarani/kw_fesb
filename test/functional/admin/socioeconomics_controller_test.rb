require 'test_helper'

class Admin::SocioeconomicsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Socioeconomic.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Socioeconomic.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Socioeconomic.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_socioeconomic_url(assigns(:socioeconomic))
  end

  def test_edit
    get :edit, :id => Socioeconomic.first
    assert_template 'edit'
  end

  def test_update_invalid
    Socioeconomic.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Socioeconomic.first
    assert_template 'edit'
  end

  def test_update_valid
    Socioeconomic.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Socioeconomic.first
    assert_redirected_to admin_socioeconomic_url(assigns(:socioeconomic))
  end

  def test_destroy
    socioeconomic = Socioeconomic.first
    delete :destroy, :id => socioeconomic
    assert_redirected_to admin_socioeconomics_url
    assert !Socioeconomic.exists?(socioeconomic.id)
  end
end
