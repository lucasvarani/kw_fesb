require 'test_helper'

class Admin::WarningsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Warning.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Warning.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Warning.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_warning_url(assigns(:warning))
  end

  def test_edit
    get :edit, :id => Warning.first
    assert_template 'edit'
  end

  def test_update_invalid
    Warning.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Warning.first
    assert_template 'edit'
  end

  def test_update_valid
    Warning.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Warning.first
    assert_redirected_to admin_warning_url(assigns(:warning))
  end

  def test_destroy
    warning = Warning.first
    delete :destroy, :id => warning
    assert_redirected_to admin_warnings_url
    assert !Warning.exists?(warning.id)
  end
end
