require 'test_helper'

class Admin::ScientificInitiationsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ScientificInitiation.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ScientificInitiation.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ScientificInitiation.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_scientific_initiation_url(assigns(:scientific_initiation))
  end

  def test_edit
    get :edit, :id => ScientificInitiation.first
    assert_template 'edit'
  end

  def test_update_invalid
    ScientificInitiation.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ScientificInitiation.first
    assert_template 'edit'
  end

  def test_update_valid
    ScientificInitiation.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ScientificInitiation.first
    assert_redirected_to admin_scientific_initiation_url(assigns(:scientific_initiation))
  end

  def test_destroy
    scientific_initiation = ScientificInitiation.first
    delete :destroy, :id => scientific_initiation
    assert_redirected_to admin_scientific_initiations_url
    assert !ScientificInitiation.exists?(scientific_initiation.id)
  end
end
