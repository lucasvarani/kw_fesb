require 'test_helper'

class Admin::CataloguingDataControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => CataloguingData.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    CataloguingData.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    CataloguingData.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_cataloguing_data_url(assigns(:cataloguing_data))
  end

  def test_edit
    get :edit, :id => CataloguingData.first
    assert_template 'edit'
  end

  def test_update_invalid
    CataloguingData.any_instance.stubs(:valid?).returns(false)
    put :update, :id => CataloguingData.first
    assert_template 'edit'
  end

  def test_update_valid
    CataloguingData.any_instance.stubs(:valid?).returns(true)
    put :update, :id => CataloguingData.first
    assert_redirected_to admin_cataloguing_data_url(assigns(:cataloguing_data))
  end

  def test_destroy
    cataloguing_data = CataloguingData.first
    delete :destroy, :id => cataloguing_data
    assert_redirected_to admin_cataloguing_data_url
    assert !CataloguingData.exists?(cataloguing_data.id)
  end
end
