require 'test_helper'

class ExAlunosControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => Ex-aluno.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    Ex-aluno.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    Ex-aluno.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to ex_aluno_url(assigns(:ex_aluno))
  end

  def test_edit
    get :edit, :id => Ex-aluno.first
    assert_template 'edit'
  end

  def test_update_invalid
    Ex-aluno.any_instance.stubs(:valid?).returns(false)
    put :update, :id => Ex-aluno.first
    assert_template 'edit'
  end

  def test_update_valid
    Ex-aluno.any_instance.stubs(:valid?).returns(true)
    put :update, :id => Ex-aluno.first
    assert_redirected_to ex_aluno_url(assigns(:ex_aluno))
  end

  def test_destroy
    ex_aluno = Ex-aluno.first
    delete :destroy, :id => ex_aluno
    assert_redirected_to ex_alunos_url
    assert !Ex-aluno.exists?(ex_aluno.id)
  end
end
