KwFesb::Application.routes.draw do

  resources :recipes, :only => [:show, :index]

  resources :redacaos do
    collection do
      get :redacao
      get :find_essay
    end
  end

  resources :enems

  resources(:subscribes){
    resources(:registrations, :only => [:new, :create, :show]){
      collection do
        post 'search_cep'
      end
    }
  }


  post '/tinymce_assets' => 'tinymce_assets#create'
  resources :awards, :only => [:index, :show]
  resources(:libraries, :only => [:index, :show]){
    collection do
      get 'virtual_library'
      get 'news_libraries'
      get 'tcc_nts'
      get 'sites_interessantes'
    end
    member do
      get 'news_library'
      get 'tcc_nt'
      get 'sites_interessantes_itens'
    end
  }
  resources(:pre_subscribes, :only => [:create, :show]){
    member do
      get 'class_new'
    end
  }
  resources :sheets, :only => [:new, :create, :show]
  resources :password_resets, :only => [:new, :create, :edit, :update]
  resources :displays, :only => [:index, :show]
  resources :researches, :only => [:index, :show]
  resources :works, :only => [:new, :create, :show]
  resources :contacts
  resources (:transfers){
    collection do
      get :download
    end
  }
  resources :ex_alunos , :only => [:new, :create, :show]
  resources(:channel_students, :only => [:index, :show]){
    resources :displays, :only => [:index, :show]
  }
  resources :social_actions, :only => [:index, :show]
  resources :commissions, :only => [:index, :show]
  resources :class_names, :only => [:index, :show]
  resources(:class_categories, :only => [:index, :show]){
    collection do
      get 'innership_standard'
    end
  }
  resources :institutions, :only => [:index, :show]
  resources (:homes, :only => [:index]){
    collection do
      get :search
      get :virtual_tour
      get :fesb50anos
      get :sae
      get :teste
    end
  }
  resources :user_sessions
  resources :edicts
  resources :regiments
  resources :standings
  resources :president_galleries

  resources (:simulateds, :only => [:index]){
    collection do
      post 'confirmation'
    end
  }

  match 'fesb50anos' => "homes#fesb50anos"
  match 'sae' => "homes#sae"
  match 'confirmacao' => "simulateds#confirmation"
  match 'confirmesuapresenca' => "simulateds#index"

  resources (:exams){
    collection do
      get 'simulador'
      get 'informacoes'
      get 'inscricao'
      get 'update_prices'
      get 'update_dates'
      get 'login'
      get 'ticket'
      get 'find_cpf'
      post 'find_result'
      get 'my_email'
      post 'find_my_email'
    end
  }
  resources (:socioeconomics){
    collection do
      get 'update_cities'
    end
  }
  resources (:users){
    member do
      get 'payment'
      post 'payment'
    end

    collection do
      post 'search_cep'
      get 'update_second_option'
    end
  }

  root :to => 'homes#index'
  match 'tourvirtual' => "homes#virtual_tour"
  match 'register' => "users#new"
  match 'login' => 'user_sessions#new'
  match 'logout' => 'user_sessions#destroy'

  namespace(:admin){
    resources :recipes
    resources :enems
    resources :redacaos
    resources :librarie_tab_names
    resources :itinerant_exams
    resources :registrations
    resources :file_imports
    resources (:sheets){
      member do
        get 'export_csv'
      end
    }
    resources :pre_subscribes
    resources :cataloguing_data
    resources :highlights
    resources :schedules
    resources (:subscribes){
        member do
	        get 'paid'
	        get 'cpaid'
	        get 'adjusts_ticket_date'
	        #ajuste de informação do usuario
	        get 'edit_user_data'
	        put 'user_data_update'
	        #ajuste de informação do vestibular
	        get 'edit_subscribe_data'
	        put 'subscribe_data_update'
	        #ajuste de informação socieconomica
	        get 'edit_socieconomic_data'
	        put 'socieconomic_data_update'
	      end
	      collection do
          
          get 'simuladao'
          get 'confirmeds'
          get 'notconfirmeds'
	        get 'management_report'
	        get 'management_porta'
          get 'management_class_room'
	        get 'management_presenca'
	        get 'update_dates'
          get 'pne_subscribes'
          get 'socieconomic_report'
	        #pesquisa data em relação ao vestibular
	        get 'search_date'
	        get 'update_cities'
	      end

    }
    resources :cities
    resources :class_styles
    resources :cities
    resources :class_styles
    resources (:categories){
      resources (:items){
          resources :item_images
      }
    }
    resources :teachers
    resources (:class_names){
        member do
          get "adjusts_period"
          put "save_adjusts_period"
          post "save_adjusts_period"
        end
        collection do
          get "disable_all_subscribe"
          get "enable_all_subscribe"
        end
        resources :class_images
    }
    resources :class_categories
    resources (:warnings){
        resources :warning_images
    }
    resources :type_warnings
    resources :works
    resources :contacts
    resources :ex_alunos
    resources :departments

    resources :categories

    resources :item_images
    resources :items
    resources :categories
    resources (:items){
        resources :item_images
    }

    resources(:exams){
      member do
        get :report
        get :presence
        get :import_file
        get :launch_note
        put :processing_note
        get :show_note
        get :ajust_message
      end
      collection do
        put :print_selected
      end
      resources(:exam_dates){
        member do
          get :report
          get :presence
          get :not_paid
          get :launch_note
          put :processing_note
          get :show_note
        end
        collection do
          get :results
        end
      }


    }

    resources :socioeconomics

    resources (:awards){
       resources :award_galleries
    }
    resources :scientific_initiations
    resources :statics, :only => [:index]

    #INSERIDO O MÓDULO DE CONTROLE DE USUARIOS DO CMS
    resources :users

    root :to => 'statics#index'
  }

  match '/500' => 'errors#error_500'
  match '/404' => 'errors#error_404'

  #VESTIBULAR

  # match 'vestibular' => "exams#index"
  # match 'vestibular/inscricao' => "exams#inscricao"
  # match 'simuladao' => 'exams#redirect_simuladao'
  # match 'vestibular/login' => 'exams#login'
  # match 'vestibular/agendado' => 'exams#agendado'
  # match 'vestibular/enem' => 'enems#new'
  # match 'enem' => 'enems#new'
  # match 'enem' => 'exams#redirect_enem'
  # match 'vestibular/tradicional' => 'exams#redirect_tradicional'
  # match 'vestibular/informacoes' => 'exams#informacoes'
  # match 'vestibular/simulador' => 'exams#simulador'
  # match 'vestibular/editais' => 'exams#edital'
  # match 'vestibular-inverno' => 'redacaos#new'
  # match 'redacao' => 'redacaos#redacao'

end